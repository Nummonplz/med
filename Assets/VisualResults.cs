﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VisualResults : MonoBehaviour {

    public Sprite[] Sprites;
    public float Sec = 1;

    Coroutine current;

    public void ShowVisual(int i)
    {
        this.GetComponent<Image>().sprite = Sprites[i];
        if (current != null)
            StopCoroutine(current);
        current = StartCoroutine(Show());
    }

    private IEnumerator Show()
    {

        yield return new WaitForSeconds(Sec);
        this.gameObject.SetActive(false);
    }
}
