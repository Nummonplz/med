﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChartCover : MonoBehaviour
{

    public Text noText;
    public Slider Slider;
    public Text scoreText;

    public void setUI(string time, float per, int score)
    {
        Slider.enabled = true;
        noText.text = "" + time;
        Slider.value = per;
        Slider.enabled = false;
        scoreText.text = "" + score;
    }

}
