﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEmotion : MonoBehaviour
{
    public delegate void OnValueChange(string name);
    public static event OnValueChange ValueChange;
    Button button;

    void Start()
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(delegate { OnClick(); });
    }


    void OnClick()
    {
        if (ValueChange != null)
        {
            ValueChange(this.name);
        }
    }
}
