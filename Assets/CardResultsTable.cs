﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardResultsTable : MonoBehaviour {

    Image[] images;
    public Sprite[] Sprites;

	void init()
    {
        int i = this.transform.childCount;
        images = new Image[i];
        for (int j = 0; j < i; j++)
        {
            images[j] = this.transform.GetChild(j).transform.GetComponent<Image>();
        }
    }

    public void setUI(CardGame cardGame)
    {
        init();
        images[0].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = cardGame.sprite;
        images[1].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = getPointSprite(cardGame.point);
    }

    Sprite getPointSprite(Point point)
    {
        int index;
        switch (point)
        {
            case Point.correct:
                index = 0;
                break;
            case Point.incorrect:
                index = 1;
                break;
            default:
                index = 2;
                break;
        }
        return Sprites[index];
    }
}
