﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResultCard : MonoBehaviour
{
    public Image image;
    public Text secText;
    public CardGame cardGame;

	void Start () {
		
	}

    public void setCover(CardGame cg)
    {
        cardGame = cg;
        image.sprite = cg.sprite;
        secText.text = "" + cg.time.ToString("0.00") + "s";
    }
}
