﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionPotentialCover : MonoBehaviour {

    public Text title;
    public Toggle[] Toggles;
    QuestionPotential cardGame;

	void Start () 
    {
        Toggles[0].onValueChanged.AddListener(delegate {
            OnValueChange(Toggles[0]);
        });
        Toggles[1].onValueChanged.AddListener(delegate {
            OnValueChange(Toggles[1]);
        });
	}

    public void setUI(QuestionPotential cg)
    {
        cardGame = cg;
        setPoint(cg.point);
    }

    void OnValueChange(Toggle toggle)
    {
        if(toggle.isOn)
        {
            switch (toggle.name)
            {
                case "c":
                    cardGame.point = Point.correct;
                    break;
                case "ic":
                    cardGame.point = Point.incorrect;
                    break;
                default:
                    break;
            }
            setPoint(cardGame.point);
        }
    }

    void setPoint(Point p)
    {
        switch (p)
        {
            case Point.correct:
                Toggles[0].isOn = true;
                Toggles[1].isOn = false;
                break;
            case Point.incorrect:
                Toggles[1].isOn = true;
                Toggles[0].isOn = false;
                break;
            case Point.none:
                Toggles[0].isOn = false;
                Toggles[1].isOn = false;
                break;
            default:
                break;
        }
    }
}
