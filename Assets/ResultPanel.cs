﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : MonoBehaviour
{

    PatientData patientData;
    SessionData sessionData;

    public Text TotalTypeResultsText;
    [SerializeField] Transform contentCorrent;
    [SerializeField] Transform contentIncorrent;
    [SerializeField] Text headTitleText;
    [SerializeField] Text hnText;
    [SerializeField] Text nameText;
    [SerializeField] Button doneButton;
    public GameObject[] boderButton;

    public Text Deckname;
    public Text nCorrect;
    public Text nIncorrect;
    public Text totalTime;
    public Text Date;

    List<GameObject> Cards;

    int nC;
    int nIc;
    float totalTimePlay;

    public ChartManager chartManager;
    public CardResultsTableManager CardResultsTableManager;

    void Start()
    {
        doneButton.onClick.AddListener( delegate { OnDone(); } );
    }

    void OnEnable()
    {
        ShowTotal("Correct");
        sessionData = UserInfo.instance.sessionData;
        patientData = UserInfo.instance.patientData;

        chartManager.Hide(true);
        CardResultsTableManager.Hide(true);

        ClearCards();
        CreateCard();

        setPatientData();
    }

    void setPatientData()
    {
        print("Set Data");
        if (UserInfo.instance.currentDeck == null)
            return;
        patientData = UserInfo.instance.patientData;
        nameText.text = " " + patientData.pt.name;
        hnText.text = "HN/AN : " + patientData.pt.hn;
        Deckname.text = string.Format("ชื่อเกมส์ ({0}) : {1}/{2}/{3}", UserInfo.instance.currentDeck.name ,System.DateTime.Now.Day,System.DateTime.Now.Month,System.DateTime.Now.Year);
        totalTime.text = "Total Time " + totalTimePlay.ToString("0.00");
        Date.text = "" + System.DateTime.Today;
        nCorrect.text = "" + nC + " / " + (sessionData.maxCard - UserInfo.instance.currentDeck.offset);
        nIncorrect.text = "" + nIc + " / " + (sessionData.maxCard - UserInfo.instance.currentDeck.offset);
        headTitleText.text = string.Format("Session No.{0} :",sessionData.no);
    }

    void CreateCard()
    {
        Cards = new List<GameObject>();
        nC = 0;
        nIc = 0;
        totalTimePlay = 0;
        foreach (CardGame card in sessionData.cards)
        {
            switch (card.point)
            {
                case Point.correct:
                    nC++;
                    setCard(card, 0);
                    break;
                case Point.incorrect:
                    nIc++;
                    setCard(card, 1);
                    break;
                case Point.none:
                    break;
                default:
                    break;
            }
            totalTimePlay += card.time;
        }
    }

    public void ShowTotal(string type)
    {
        switch (type)
        {
            case "Correct":
                TotalTypeResultsText.text = "TOTAL\nCORRECT ANSWERS";
                contentCorrent.transform.parent.transform.parent.gameObject.SetActive(true);
                contentIncorrent.transform.parent.transform.parent.gameObject.SetActive(false);
                boderButton[0].SetActive(true);
                boderButton[1].SetActive(false);
                break;
            case "Incorrect":
                TotalTypeResultsText.text = "TOTAL\nINCORRECT ANSWERS";;
                contentIncorrent.transform.parent.transform.parent.gameObject.SetActive(true);
                contentCorrent.transform.parent.transform.parent.gameObject.SetActive(false);
                boderButton[0].SetActive(false);
                boderButton[1].SetActive(true);
                break;
            default:
                break;
        }
    }

    void setCard(CardGame card,int index)
    {
        GameObject rc = Instantiate(LoadResources.Load("prefap/ResultCard"));
        if(index == 0)
            rc.transform.SetParent(contentCorrent, false);
        else if(index == 1)
            rc.transform.SetParent(contentIncorrent, false);
        ResultCard rp = rc.GetComponent<ResultCard>();
        rp.setCover(card);
        Cards.Add(rc);
    }

    void ClearCards()
    {
        if (Cards == null)
            return;
        int c = Cards.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(Cards[i].gameObject);
        }
    }

    public void OnDone()
    {
        if (UserInfo.instance.patientData.HistorySessionData == null)
            UserInfo.instance.patientData.HistorySessionData = new List<SessionData>();
        UserInfo.instance.patientData.HistorySessionData.Add(sessionData);

        patientData = null;
        sessionData = null;
        UserInfo.instance.selectDeckHandler.clearDeck();
        ControlPanel.instance.ChangePage(2);
    }

}
