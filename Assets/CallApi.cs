﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallApi : MonoBehaviour 
{
    public delegate void Callback(bool success);
    public static CallApi instance;
    public Text Textloading;
    public List<ApiDetail> apiDetails;

    Coroutine currentAction;

	void Start () 
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            apiDetails = new List<ApiDetail>();
        }
        Textloading.fontSize = 10;
    }
	
    public void callBegin(string name)
    {
        waitingPanel.instance.OpenPanel(true);
        int index = apiDetails.FindIndex((x) => x.name == name);
        if(index == -1)
        {
            ApiDetail apiDetail = new ApiDetail(name);
            apiDetail.AddBegin();
            apiDetails.Add(apiDetail);
        }
        else
        {
            apiDetails[index].AddBegin();
        }
    }

    public void callBegin(string name,int amount)
    {
        waitingPanel.instance.OpenPanel(true,amount);
        int index = apiDetails.FindIndex((x) => x.name == name);
        if (index == -1)
        {
            ApiDetail apiDetail = new ApiDetail(name);
            apiDetail.Max += amount;
            apiDetails.Add(apiDetail);
        }
        else
        {
            apiDetails[index].Max += amount;
        }
    }

    public void callBack(string name,bool status,Callback back = null)
    {
        
        int index = apiDetails.FindIndex((x) => x.name == name);
        if (index != -1)
        {
            waitingPanel.instance.OpenPanel(false,1);
            if (apiDetails[index].AddEnd(status))
            {                
                //print(apiDetails[index].name + " nApi : " + apiDetails[index].nSuccess + " / " + apiDetails[index].Max);
                if (back != null)
                    back(apiDetails[index].apiSuccess());
                apiDetails[index].Clear();
            }
            //print(apiDetails[index].name + " : " + apiDetails[index].nSuccess + " / " + apiDetails[index].Max);
            Textloading.text = "";
            foreach (ApiDetail api in apiDetails)
            {
                if (api.Max == 0)
                    continue;
                Textloading.text += "" + api.name + " : " + api.nSuccess + " / " + api.Max + "\n";
            }           
        }
    }
}

[System.Serializable]
public class ApiDetail
{
    public string name;
    public int current;
    public int Max;
    public int nSuccess;
    public int nFailed;
    public float t;
    public ApiDetail(string n)
    {
        name = n;
        current = 0;
        Max = 0;
        t = 0;
    }

    public void AddBegin()
    {
        Max++;
    }

    public bool AddEnd(bool status)
    {
        if (status)
            nSuccess++;
        else
            nFailed++;
        
        current++;
        return (current >= Max);
    }

    public void Clear()
    {
        current = 0;
        Max = 0;
        nSuccess = 0;
        nFailed = 0;
        t = 0;
    }

    public bool apiSuccess()
    {
        return nSuccess >= Max;
    }
}
