﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardResultsTableManager : MonoBehaviour {

    public GameObject prefab;
    public Transform content;
    SessionData sessionData;
    List<GameObject> allResult;

    private void OnEnable()
    {
        if (UserInfo.instance.sessionData == null)
        {
            this.gameObject.SetActive(false);
            return;
        }
        SetUp();
    }

    public void SetUp()
    {
        sessionData = UserInfo.instance.sessionData;
        clearTemp();
        createCardRusults();
    }

    void createCardRusults()
    {
        int nCard = sessionData.cards.Count;
        for (int i = 0; i < nCard; i++)
        {
            GameObject result = Instantiate(prefab);
            result.GetComponent<CardResultsTable>().setUI(sessionData.cards[i]);
            result.transform.SetParent(content.transform,false);
            allResult.Add(result);
        }
    }

    void clearTemp()
    {
        if (allResult == null)
            allResult = new List<GameObject>();
        int c = allResult.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(allResult[i].gameObject);
        }
    }

    public void Hide(bool hide)
    {
        this.gameObject.SetActive(!hide);
    }
}
