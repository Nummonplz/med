﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class waitingPanel : MonoBehaviour {

    public static waitingPanel instance;
    GameObject windowScreen;
    int cEvent;
	void Start () 
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            cEvent = 0;
            instance = this;
            windowScreen = this.transform.GetChild(0).gameObject;
            OpenPanel(false);
        }
	}
	
    public void OpenPanel(bool on,int i = 1)
    {      
        if(on)
        {
            if (cEvent == 0)
            {
                windowScreen.SetActive(on);
            }
            add(i);
        }
        else
        {
            remove(i);
            if (cEvent == 0)
            {
                windowScreen.SetActive(on);
            }
        }
    }

    void add(int i)
    {
        cEvent+=i;
    }

    void remove(int i)
    {
        cEvent-=i;
        if (cEvent < 0)
            cEvent = 0;
    }
}
