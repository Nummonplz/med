﻿using UnityEngine;
using UnityEngine.UI;
using System;
public class DataInput : MonoBehaviour
{
    public delegate void OnEdit(string name,string ipfText);
    public static event OnEdit OnEditEnd;

    public delegate string OnLoad(string name);
    public static event OnLoad OnLoadText;

    public InputField ipf;
    void Start()
    {
        OnLoadTexts();
    }

    void OnEditTextEnd()
    {
        if(OnEditEnd != null)
        {
            OnEditEnd(this.name,ipf.text);
        }
    }

    public void OnLoadTexts()
    {
        if(OnLoadText != null)
        {
            ipf.text = OnLoadText(this.name);
        }
    }

    public void setUp()
    {
        ipf = transform.GetChild(1).GetComponent<InputField>();
        ipf.onEndEdit.AddListener(delegate { OnEditTextEnd(); });
    }
}


public class PatientControl : MonoBehaviour
{
    public Transform content;
    public Transform dataSec1;

    public GameObject[] TabBottom;
    public GameObject Lock;
    public GameObject LockList;

    public Button startButton;
    public Button editButton;
    public Button deleteButton;
    public Button doneButton;
    public Button cancleButton;

    public InputField idName;

    public SelectDeckHandler selectDeckHandler;
    PatientData pd;
    Patient temp;
    PatientCover currentCover;
    public SexInfo sexInfo;
    void Start()
    {
        startButton.onClick.AddListener(delegate
        {
            OnStartClick();
        });
        editButton.onClick.AddListener(delegate
        {
            OnEdit();
        });
        deleteButton.onClick.AddListener(delegate
        {
            OnDelete();
        });
        doneButton.onClick.AddListener(delegate
        {
            OnDone();
        });
        cancleButton.onClick.AddListener(delegate
        {
            OnCancle();
        });
        setKeepData();
        content.transform.gameObject.SetActive(false);
    }

    public void OnStartClick()
    {
        if (pd == null)
            return;
        selectDeckHandler.setDeckList(pd);
        UserInfo.instance.patientData = this.pd;
        ControlPanel.instance.ChangePage(0);
    }

    void setKeepData()
    {
        int c = dataSec1.transform.childCount;
        for (int i = 0; i < c; i++)
        {
            if (dataSec1.transform.GetChild(i).gameObject.name != "BottonTab" && dataSec1.transform.GetChild(i).gameObject.name != "sex")
            {
                dataSec1.transform.GetChild(i).gameObject.AddComponent<DataInput>();
                dataSec1.transform.GetChild(i).gameObject.GetComponent<DataInput>().setUp();
            }

            if(dataSec1.transform.GetChild(i).gameObject.name == "sex")
            {
                sexInfo = dataSec1.transform.GetChild(i).gameObject.GetComponent<SexInfo>();
            }
        }
    }

    void setLoadText()
    {
        int c = dataSec1.transform.childCount;
        for (int i = 0; i < c; i++)
        {
            if (dataSec1.transform.GetChild(i).gameObject.name != "BottonTab" && dataSec1.transform.GetChild(i).gameObject.name != "sex")
            {
                dataSec1.transform.GetChild(i).GetComponent<DataInput>().OnLoadTexts();
            }
            else if(dataSec1.transform.GetChild(i).gameObject.name == "sex")
            {
                sexInfo.sex = pd.pt.gender;
                sexInfo.SetCover();
            }
        }
    }

    void OnEnable()
    {
        if (UserInfo.instance != null)
            if (UserInfo.instance.Logined)
                UserInfo.instance.doctorCover.refreshPatientList();
        DataInput.OnEditEnd += editData;
        DataInput.OnLoadText += loadData;
        PatientCover.OnChangePanel += ChangePatient;
        BottomMode(0);

    }

    void OnDisable()
    {
        DataInput.OnEditEnd -= editData;
        DataInput.OnLoadText -= loadData;
        PatientCover.OnChangePanel -= ChangePatient;
    }

    void editData(string name, string ipfText)
    {
        switch (name)
        {
            case "Name":
                pd.pt.name = ipfText;
                if(currentCover != null)
                    currentCover.setCover();
                break;
            case "hn":
                pd.pt.hn = ipfText;
                if(currentCover != null)
                    currentCover.setCover();
                break;
            case "remark":
                pd.pt.remark = ipfText;
                break;
            case "phone":
                pd.pt.phone = ipfText;
                break;
            case "age":
                pd.pt.age = ipfText;
                break;
            case "sex":
                if (currentCover != null)
                    currentCover.setCover();
                pd.pt.gender = sexInfo.sex;
                break;
        }
    }

    string loadData(string name)
    {
        if (pd == null)
            return "";
        switch (name)
        {
            case "Name":
                return pd.pt.name;
            case "hn":
                return pd.pt.hn;
            case "phone":
                return pd.pt.phone;
            case "age":
                return pd.pt.age;
            case "remark":
                return pd.pt.remark;
            case "sex":
                return pd.pt.gender;
            default:
                return "";
        }
    }

    void ChangePatient(PatientData currentPD, PatientCover pc)
    {
        if (!content.transform.gameObject.activeSelf)
        {
            content.transform.gameObject.SetActive(true);
        }

        if (currentCover != null)
        {
            Color color = currentCover.gameObject.GetComponent<Image>().color;
            color.a = 0.1f;
            currentCover.gameObject.GetComponent<Image>().color = color;
        }

        currentCover = pc;
        Color color2 = pc.gameObject.GetComponent<Image>().color;
        color2.a = 0.5f;
        pc.gameObject.GetComponent<Image>().color = color2;

        pd = currentPD;
        sexInfo.pd = pd;
        UserInfo.instance.patientData = currentPD;
        setLoadText();
        setToggle();
        BottomMode(0);
     }

    void setToggle()
    {
        /*if(pd.listOT == null || pd.listOT.Count <= 0)
            pd.listOT = dataSec2.gameObject.GetComponent<createToggle>().resetOT();
        dataSec2.gameObject.GetComponent<createToggle>().resetToggle(pd.listOT);*/
    }

    public void OnEdit()
    {
        if (pd == null)
            return;
        CopyDataToTemp(pd.pt);
        BottomMode(1);
    }

    public void OnCreate()
    {
        Reset();
    }

    private void Reset()
    {
        OnresetUI();
        newPaitient();

        currentCover = null;
        setLoadText();
        setToggle();
        BottomMode(1);
    }

    void OnresetUI()
    {
        if (!content.transform.gameObject.activeSelf)
        {
            content.transform.gameObject.SetActive(true);
        }

        if (currentCover != null)
        {
            Color color = currentCover.gameObject.GetComponent<Image>().color;
            color.a = 0.1f;
            currentCover.gameObject.GetComponent<Image>().color = color;
        }    
    }

    void newPaitient()
    {
        pd = new PatientData();
        pd.pt = new Patient();
        pd.pt.name = "";
        pd.pt.hn = "";
        pd.pt.an = "";
        pd.pt.image = "";
        pd.pt.phone = "";
        pd.pt.email = "";

        pd.pt.age = "";
        pd.pt.gender = "male";
        pd.pt.remark = "";
        sexInfo.pd = pd;
        sexInfo.SetCover();
    }

    public void OnDelete()
    {
        if (pd == null)
            return;
        MessageBox.instance.showMsg("คุณแน่ใจที่จะลบข้อมูลหรือไม่",delegate
        {
            CallApi.instance.callBegin("DeletePatient");
            CallApi.instance.callBegin("DeletePatient");
            WebServiceManager.instance.DeletePatient(pd.pt.userid,
                                                     (success) =>
                                                     {
                                                         //print(success);
                                                         CallApi.instance.callBack("DeletePatient", success);
                                                         WebServiceManager.instance.PatientList(UserInfo.instance.User.id, false,
                                                                                                (success2) =>
                                                                                                {
                                                                                                    //print(success2);
                                                                                                    CallApi.instance.callBack("DeletePatient", success2, DeleteSuccess);
                                                                                                });
                                                     });
        });
    }

    void DeleteSuccess(bool s)
    {
        if (s)
        {
            UserInfo.instance.doctorCover.refreshPatientList();
            content.transform.gameObject.SetActive(false);
            pd = null;
        }
    }

    public void OnDone()
    {
        if (temp != null)
        {
            if (!OnValidateAddPatient(pd.pt.name, pd.pt.hn ,pd.pt.age))
                return;
            MessageBox.instance.showMsg("คุณแน่ใจที่จะบันทึกข้อมูลหรือไม่",delegate
            {
                string[] breakApart = pd.pt.name.Split(' ');
                string[] name = { "", "" };
                if (breakApart.Length > 0)
                    name[0] = breakApart[0];
                if (breakApart.Length > 1)
                    name[1] = breakApart[1];
                
                CallApi.instance.callBegin("EditPatient");
                WebServiceManager.instance.EditPatient(pd.pt.userid,                                                     
                                                       name[1],
                                                       name[0],
                                                       pd.pt.hn,
                                                       pd.pt.phone,
                                                       pd.pt.gender,
                                                       pd.pt.age,
                                                       pd.pt.remark,
                                                       (succuss) =>
                {
                    CallApi.instance.callBack("EditPatient", succuss, (success) =>
                  {
                      if (success)
                          BottomMode(0);
                  });
                });
            });
        }
        else
        {
            string[] breakApart = pd.pt.name.Split(' ');
            string[] name = { "", "" };
            string username = "";
            if (breakApart.Length > 0)
            {
                name[0] = breakApart[0];
            }
            if (breakApart.Length > 1)
            {
                name[1] = breakApart[1];
            }

            username = name[0] + name[1] + pd.pt.hn;
            print(pd.pt.hn);
            if (!OnValidateAddPatient(pd.pt.name, pd.pt.hn,pd.pt.age))
                return;

            MessageBox.instance.showMsg("คุณแน่ใจที่จะบันทึกข้อมูลหรือไม่",delegate
            {
                print("Create Patient");

                WebServiceManager.instance.NewPatient(
                                                      UserInfo.instance.User.organization_id,
                                                      UserInfo.instance.User.department_id,
                                                      "" + name[1], // lastname
                                                      "" + name[0], //fullname
                                                      "" + pd.pt.hn, // hn *
                                                      "" + pd.pt.phone, // phone *
                                                      "" + pd.pt.gender,
                                                      "" + pd.pt.age,
                                                      "" + pd.pt.remark,
                                              (success) =>
                                              {
                                                  if (success)
                                                  {
                                                      WebServiceManager.instance.PatientList(UserInfo.instance.User.id, false,
                                                                                             delegate
                                                                                             {
                                                                                                 UserInfo.instance.doctorCover.refreshPatientList();
                                                                                             });
                                                      BottomMode(0);
                                                  }
                                              });
            });
        }
    }

    private bool OnValidateAddPatient(string namePatient,string hn,string age)
    {
        bool pass = false;
        string msgError = "";
        int i;
        if (namePatient.Length <= 0)
        {
            msgError += "กรุณาระบุ ชื่อผู้ใช้งาน ด้วยค่ะ\n";
        }
        else if(int.TryParse(namePatient[0].ToString(),out i))
        {
            msgError += "ชื่อผู้ใช้งานตัวอักษรแรกไม่ควรเป็นตัวเลข\n";
        }

        pass = msgError.Length <= 0;

        if (!pass)
        {
            MessageBox.instance.showError(msgError);
        }
        
        return pass;
    }

    void BottomMode(int i)
    {
        switch (i)
        {
            case 0 :
                Lock.SetActive(true);
                TabBottom[0].SetActive(true);
                TabBottom[1].SetActive(false);
                LockList.SetActive(false);
                break;
            case 1 :
                Lock.SetActive(false);
                TabBottom[1].SetActive(true);
                TabBottom[0].SetActive(false);
                LockList.SetActive(true);
                break;
            default:
                break;
        }
    }

    void OnValueInputChange(string name)
    {

    }

    void OnCancle()
    {
        if (temp != null)
            CopyDataToCurrent(temp);
        else
            content.transform.gameObject.SetActive(false);

        if (currentCover != null)
            currentCover.setCover();

        setLoadText();
        BottomMode(0);
    }

    void CopyDataToTemp(Patient copyform)
    {
        Patient own = new Patient();
        own.userid = copyform.userid;
        own.name = copyform.name;
        own.phone = copyform.phone;
        own.hn = copyform.hn;
        own.age = copyform.age;
        own.email = copyform.email;
        own.depart = copyform.depart;
        own.image = copyform.image;
        own.gender = copyform.gender;
        own.remark = copyform.remark;
        temp = own;
    }

    void CopyDataToCurrent(Patient copyform)
    {
        Patient own = new Patient();
        own.userid = copyform.userid;
        own.name = copyform.name;
        own.phone = copyform.phone;
        own.hn = copyform.hn;
        own.age = copyform.age;
        own.email = copyform.email;
        own.depart = copyform.depart;
        own.image = copyform.image;
        own.gender = copyform.gender;
        own.remark = copyform.remark;
        pd.pt = own;
        temp = null;
    }

    bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            print(addr);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }
}
