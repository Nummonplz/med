﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class listCardSelect : MonoBehaviour
{
    Deck deck;
    public SessionControl session;
    public Transform content;
    public List<GameObject> cardsSelect;
    void OnEnable()
    {
        CardSelect.OnClick += setDisable;
        deck = session.deck;
        if(content.childCount <= 0)
        {
            CreateListCard();
        }
    }

    void OnDisable()
    {
        CardSelect.OnClick -= setDisable;
        if (cardsSelect != null)
        {
            ClearListCard();
        }
    }

    void CreateListCard()
    {
        cardsSelect = new List<GameObject>();
        int i = 0;
        foreach (Card c in deck.listCard)
        {
            
            if (UserInfo.instance.cardsInfo.ContainsKey(c.ID.ToString()))
            {
                CardInfo cardInfo = UserInfo.instance.cardsInfo[c.ID.ToString()];
                GameObject newCard = Instantiate(LoadResources.Load("prefap/CardSelect")) as GameObject;
                newCard.transform.SetParent(content, false);

                if(SessionControl.instance.gamePlay.language == Language.EN)
                    newCard.GetComponent<CardSelect>().setUp(cardInfo.card_name_en, cardInfo.img, i);
                else if(SessionControl.instance.gamePlay.language == Language.TH)
                    newCard.GetComponent<CardSelect>().setUp(cardInfo.card_name, cardInfo.img, i);
                
                cardsSelect.Add(newCard);
                i++;

                int index = SessionControl.instance.sessionData.cards.FindIndex((x) => x.id == c.ID.ToString());
                if (index != -1)
                {
                    //print("Found Index Card " + c.name);
                    newCard.GetComponent<CardSelect>().setUpPoint(SessionControl.instance.sessionData.cards[index].point);
                }
            }

        }
    }

    void ClearListCard()
    {
        int c = cardsSelect.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(cardsSelect[i].gameObject);
        }

        cardsSelect = new List<GameObject>();
    }

    public void Hide(bool on)
    {
        this.gameObject.SetActive(on);
    }

    public void OnClick()
    {
        bool open = this.gameObject.active;
        this.gameObject.SetActive(!open);
    }

    void setDisable(int i)
    {
        Hide(false);
    }
}
