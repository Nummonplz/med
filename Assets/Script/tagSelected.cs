﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class tagSelected : MonoBehaviour
{
    public delegate void OnButton(string name);
    public static event OnButton Click;
    Button button;
    void Start()
    {
        button = this.GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(delegate { OnClick(); });
    }

    void OnClick()
    {
        if (Click != null)
            Click(this.name);
    }
}
