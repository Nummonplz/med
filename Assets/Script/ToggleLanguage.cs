﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleLanguage : MonoBehaviour
{

    public delegate void OnValueChange(Toggle toggle);
    public static event OnValueChange ValueChange;

    public Sprite[] sprites;
    Toggle toggle;

    void Start()
    {
        toggle = this.GetComponent<Toggle>();
        toggle.name = this.name;
        toggle.onValueChanged.AddListener( delegate { OnClick(); });
    }

    private void OnEnable()
    {
        /*if (toggle != null)
        {
            print(toggle.name + toggle.isOn.ToString());
            if (!toggle.isOn)
            {
                toggle.isOn = true;
            }
        }
        OnClick();*/
    }

    void OnClick()
    {
        if (toggle == null)
            return;
        if(ValueChange != null)
        {
            print(toggle.name);
            ValueChange(toggle);
            int i = toggle.isOn == true ? 0 : 1;
            if(sprites.Length > 0)
                toggle.targetGraphic.gameObject.GetComponent<Image>().sprite = sprites[i];
        }
    }
}
