﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanel : MonoBehaviour
{
    public static ControlPanel instance;
    int currentPage;
    void Start()
    {
        if (instance == null)
            instance = this;
        currentPage = 5;
        ChangePage(currentPage);
    }

    public void ChangePage(int o)
    {
        currentPage = o;
        for (int i = 0; i < this.transform.childCount; i++)
            this.transform.GetChild(i).gameObject.SetActive(currentPage == i);
    }
}
