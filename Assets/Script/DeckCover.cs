﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DeckCover : MonoBehaviour
{
    public delegate void OnToggle(Deck deck,Toggle toggle);
    public static event OnToggle ValueChange;

    [SerializeField] Text nameText;
    [SerializeField] Text numberText;
    [SerializeField] Image coverImage;

    public Deck deck;
    public Toggle toggle;

    void Start()
    {
        SetUp();
    }

    void SetUp()
    {
        if (toggle != null)
            return;
        toggle = this.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate { OnValueChange(); });
    }

    public void Create()
    {
        if (toggle != null)
            return;
        toggle = this.GetComponent<Toggle>();
        toggle.isOn = true;
        toggle.onValueChanged.AddListener(delegate { OnValueChange(); });
    }

    public void setUpCover(string name,int number)
    {
        //deck.name = name;
        refreshCover();
        SetUp();
    }

    void OnValueChange()
    {
        if (ValueChange != null)
            ValueChange(deck, toggle);
    }

    public void refreshCover()
    {
        //print(deck.name);
        nameText.text = string.Format("ชื่อเด็ค ( {0} )", deck.name);
        numberText.text = string.Format("จำนวนรูป {0} ภาพ",DeckDetail.getNumberOfCard(deck) - deck.offset);
        foreach (Card c in deck.listCard)
        {
            if (UserInfo.instance.cardsInfo.ContainsKey(c.ID.ToString()))
            {
                coverImage.sprite = UserInfo.instance.cardsInfo[c.ID.ToString()].img;
                break;
            }
        }
            
    }

    void OnEnable()
    { 
        if(deck != null)
            refreshCover();
    }
}
