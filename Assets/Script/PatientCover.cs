﻿using UnityEngine;
using UnityEngine.UI;

public class PatientCover : MonoBehaviour
{
    public delegate void OnChange(PatientData pd,PatientCover pc);
    public static event OnChange OnChangePanel;

    //temp
    public PatientData pd;

    public GameObject obj;

    public Image profileImage;
    public Image profileImageBG;
    public Text nameText;
    public Text hnText;
    public Sprite[] sexImages;
    public Sprite[] sexImagesBG;
    Button btn;
	void Start ()
    {
        setCover();
        btn = this.GetComponent<Button>();
        btn.onClick.AddListener(delegate { OnChangePanelPatient(); });
        //OnChangePanelPatient();
    }

    public void setCover()
    {
        nameText.text = "" + pd.pt.name;
        hnText.text = "" + pd.pt.hn;
        int indexSexImage = pd.pt.gender == "male" ? 0 : 1;
        profileImage.sprite = sexImages[indexSexImage];
        profileImageBG.sprite = sexImagesBG[indexSexImage];
    }

    void OnChangePanelPatient()
    {
        if (OnChangePanel != null)
        {
            OnChangePanel(pd,this);
            setCover();
        }
    }
}
