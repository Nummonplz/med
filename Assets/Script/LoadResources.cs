﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class LoadResources
{
    public static string filePath;
    public static string result;

    public static GameObject Load(string path)
    {
        return (GameObject)Resources.Load(path);
    }

    public static Sprite LoadImage(string path)
    {
        return Resources.Load<Sprite>(path);
    }

    public static void LoadSprite(MonoBehaviour mono)
    {
#if UNITY_EDITOR
        // Editor
        string pathResources = Application.dataPath + "/Resources/fruit";
        string[] listFruit = Directory.GetFiles(pathResources);
        List<string> list = new List<string>();
        foreach (string item in listFruit)
        {
            if (!item.Contains("meta") && item.Contains("png"))
            {
                string name = item.Replace(pathResources + '\\', "");
                list.Add(name);
                mono.StartCoroutine(LoadSprite(name));
            }
        }
        // Save Name fruit 
        System.IO.Directory.CreateDirectory("Assets/StreamingAssets/");
        using (StreamWriter sw = new StreamWriter("Assets/StreamingAssets/alphabet.txt", false))
        {
            foreach (string filename in list)
            {
                sw.WriteLine(filename);
            }

        }
#endif
#if UNITY_ANDROID
        string path = "jar:file://" + Application.dataPath + "!/assets/alphabet.txt";
        WWW wwwfile = new WWW(path);
        while (!wwwfile.isDone) { }
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, "alphabet.t");
        File.WriteAllBytes(filepath, wwwfile.bytes);
        StreamReader wr = new StreamReader(filepath);
        string line;
        while ((line = wr.ReadLine()) != null)
        {
            mono.StartCoroutine(LoadSprite(line));
        }
    #endif
        
    }

    public static IEnumerator LoadSprite(string name)
    {
        string nameC = name.Replace(".png", "");
        Sprite sprite = Resources.Load<Sprite>("fruit/" + nameC);       
        //UserInfo.instance.listSprite[nameC] = sprite;
        yield break;
    }

    public static IEnumerator LoadSprite(CardInfo card,WebServiceManager.callback callback = null)
    {
        string nameC = card.card_name.Replace(".png", "");
        Sprite sprite = Resources.Load<Sprite>("fruit/" + nameC);
        card.img = sprite;
        UserInfo.instance.cardsInfo[card.id] = card;
        if (callback != null)
            callback();
        yield break;
    }



   

}
