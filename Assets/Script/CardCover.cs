﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardCover : MonoBehaviour
{
    public delegate void OnToggle(Card card,Toggle toggle);
    public static event OnToggle ValueChange;

    public Card card;
    [SerializeField] Image cardImage;
    [SerializeField] Text cardName;
    [SerializeField] Text index;
    [SerializeField] Toggle toggle;
    int currentIndex;
    public int offset = 0;
	void Start ()
    {     
        toggle = this.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate { OnValueChange(); });
    }

    private void Update()
    {
        if(toggle.isOn && card.index != currentIndex)
        {
            setIndex();    
        }
        else if(toggle.isOn == false && index.text.Length > 0)
        {
            index.text = "";
        }
    }

    public void setUp(string name,int id,Sprite spr,string cardType)
    {
        card = new Card();
        card.name = name;
        card.ID = id;
        card.type = cardType;
        cardImage.sprite = spr;
        cardName.text = name;
        RandomTag();
    }

    public void setIndex()
    {
        currentIndex = card.index - offset;
        index.text = "" + (card.index - offset);
    }

    void RandomTag()
    {
        //int i = Random.Range(0, 2);
        //card.addTag((Tag)i);
    }

    public void OnValueChange()
    {
        if (ValueChange != null)
        {
            ValueChange(card, toggle);
            setIndex();
        }
    }

  

}
