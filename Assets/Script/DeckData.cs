﻿using System.Collections.Generic;

public class Info
{
    public string name;
    public int ID;
    public int offset;
}


public class DeckList : Info
{
    public int currentDeck;
    public List<Deck> listDeck;

    public DeckList()
    {
        currentDeck = 0;
        listDeck = new List<Deck>();
    } 

    public Deck GetDeck()
    {
        return listDeck[currentDeck];
    }
}


[System.Serializable]
public class Deck : Info
{
    public List<Card> listCard;

    public Deck()
    {
        ID = -99;
        listCard = new List<Card>();
    } 
}


public class Card : Info
{
    public int index;
    public int dID;
    public string type;
    public List<string> tags;
    public Card()
    {
        tags = new List<string>();
    }

    public void addTag(Tag t)
    {
        tags.Add(t.ToString());
    }
}

public enum Tag
{
    red,
    green,
    blue
}

public static class DeckDetail
{
    public static Deck getCopy(Deck deckCopy)
    {
        Deck deck = new Deck();
        deck.name = "" + deckCopy.name;
        deck.ID = deckCopy.ID;
        deck.listCard = new List<Card>();
        foreach (Card c in deckCopy.listCard)
        {
            deck.listCard.Add(c);
        }
        return deck;
    }

    public static int getNumberOfCard(Deck deck)
    {
        return deck.listCard.Count;
    }

    public static void addDeck(DeckList dl, Deck deck)
    {
        deck.ID = dl.currentDeck;
        dl.listDeck.Add(deck);
    }
}


