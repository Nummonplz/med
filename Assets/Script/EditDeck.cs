﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class EditDeck : MonoBehaviour
{
    public Deck tempDeck;
    public Deck currentDeck;
    public ScrollRect scrollRect;
    [SerializeField] Transform content;
    [SerializeField] Transform contentSelect;
    [SerializeField] Transform contentSelected;
    [SerializeField] Transform contentSelectPanel;
    [SerializeField] Button editDone;
    [SerializeField] Button editCancel;
    [SerializeField] InputField nameText;
    public Text nOfCard;
    public Text nameOfTag;

    [SerializeField] Dictionary<int, GameObject> listCard = new Dictionary<int, GameObject>();
    [SerializeField] Dictionary<string, GameObject> listTag1 = new Dictionary<string, GameObject>();
    [SerializeField] Dictionary<string, GameObject> listTag3 = new Dictionary<string, GameObject>();
    public delegate void OnEditDeck(Deck deck);
    public static event OnEditDeck ValueChange;
    public GameObject filterPanel;

    // Data On WebService
    public int currentIndexDeck;
    public delegate void DeckOptionList(bool status);

    CardInfo[] currentCards;
    DeckOptionList OnDeckOptionList;

    void Start()
    {
        //print("Edit Start");
        LoadTag();
        listC.Clear();
        editDone.onClick.AddListener(delegate { OnEditDone(); });
        editCancel.onClick.AddListener(delegate { OnEditCancel(); });
        OnDeckOptionList += OnCallBackGetListCard;
        nameOfTag.text = "";

        if (UserInfo.instance.cardTypeByIds.Length > 0)
        {
            OnAddTag(UserInfo.instance.cardTypeByIds[0].id);
        }
    }

    void LoadCard()
    {
        int cardId = 0;
        listCard.Clear();
        //print("CardInfo : " + UserInfo.instance.cardsInfo.Count);
        foreach (CardInfo card in UserInfo.instance.cardsInfo.Values)
        {
            GameObject newCard = Instantiate(LoadResources.Load("prefap/Card")) as GameObject;
            newCard.transform.SetParent(content,false);
            CardCover cc = newCard.GetComponent<CardCover>();
            card.cardCover = cc;
            newCard.name = "" + card.card_name;
            cc.setUp(card.card_name,int.Parse(card.id),card.img,card.card_type);
            //print(card.id);
            listCard[int.Parse(card.id)] = newCard;
            cardId++;
        }
        //print("LoadCard listCard : " + listCard.Count);
        OnScreenLayout();
    }

    void OnScreenLayout()
    {
        //string xy = string.Format("h: {0} w: {1}",Screen.height,Screen.width);
        float h = 0;
        float w = 0;
        /*
        print(Camera.main.aspect);
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
            h = 230;
        }
        else if (Camera.main.aspect == 1.6)
        {
            Debug.Log("16:10");
            h = 210;
        }
        else if (Camera.main.aspect > 1.6)
        {
            Debug.Log("5:3");
            h = 210;
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
            h = 190;
        }
        else
        {
            Debug.Log("4:3");
            h = 175;
        }*/
        h = Camera.main.aspect * 120;
        w = h;
        content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(h, w);
    }

    void LoadTag()
    {
        string tag1 = "Select";
        string tag3 = "SelectPanel";
        listTag1.Clear();
        listTag3.Clear();
        foreach (CardTypeById item in UserInfo.instance.cardTypeByIds)
        {
            GameObject newTag = Instantiate(LoadResources.Load(tag1)) as GameObject;
            newTag.transform.SetParent(contentSelect,false);
            newTag.name = ""+ item.id;
            newTag.transform.GetChild(0).GetComponent<Text>().text = "" + item.name;
            listTag1[item.id] = newTag;
            GameObject newTag3 = Instantiate(LoadResources.Load(tag3)) as GameObject;
            newTag3.transform.SetParent(contentSelectPanel, false);
            newTag3.name = "" + item.id;
            newTag3.transform.GetChild(0).GetComponent<Text>().text = "" + item.name;
            listTag3[item.id] = newTag3;
            newTag3.SetActive(false);
        }
    }

    void OnSelectCard(Card card,Toggle toggle)
    {
        int i = currentDeck.listCard.FindIndex(x => x.ID == card.ID);
        if (toggle.isOn && i == -1)
        {
            currentDeck.listCard.Add(card);
            card.index = currentDeck.listCard.Count;
        }
        else if (!toggle.isOn && i != -1)
        {
            currentDeck.listCard.RemoveAt(i);
            refreshIndex();
        }

        nOfCard.text = "- / " + (currentDeck.listCard.Count - offset);
    }

    void refreshIndex()
    {
        int nCard = currentDeck.listCard.Count;
        for (int i = 0; i < nCard; i++)
        {
            currentDeck.listCard[i].index = i + 1;
        }
    }

    void OnEnable()
    {
        print("cardInfo Count : " + UserInfo.instance.cardsInfo.Count + "  || listCard Count : " + listCard.Count);
        print("ListTag : " +listTag.Count);
        print("ListTag1 : " + listTag1.Count);
        print("ListTag3 : " + listTag3.Count);
        
        if (listCard.Count <= 0)
            LoadCard();
        
        CardCover.ValueChange += OnSelectCard;
        tagSelect.Click += OnAddTag;
        tagSelected.Click += OnRemoveTag;

        if (UserInfo.instance.currentDeck != null)
        {
            tempDeck = DeckDetail.getCopy(UserInfo.instance.currentDeck);
            currentDeck = DeckDetail.getCopy(UserInfo.instance.currentDeck);
        }
        else
        {
            print("Current Deck = null");
            tempDeck = null;
            currentDeck = new Deck();
        }

        InitToggleCard();
        nameText.text = currentDeck.name;

        filterPanel.SetActive(false);

        nOfCard.text = "- / " + (currentDeck.listCard.Count - offset);

        if (UserInfo.instance.cardTypeByIds.Length > 0 && listTag1.Count > 0)
        {
            print(UserInfo.instance.cardTypeByIds[0].id);
            OnAddTag(UserInfo.instance.cardTypeByIds[0].id);
        }
    }
    int offset = 0;
    void InitToggleCard()
    {
        if (UserInfo.instance.currentDeck == null)
        {
            foreach (GameObject item in listCard.Values)
            {
                if (item.GetComponent<Toggle>().isOn)
                {
                    item.GetComponent<Toggle>().isOn = false;
                }
            }
        }
        else
        {
            offset = 0;
            foreach (DeckListInfo deck in UserInfo.instance.deckListInfos)
            {
                if(UserInfo.instance.currentDeck.ID.ToString() == deck.id)
                {
                    offset = deck.offset;
                    break;
                }
            }

            foreach (var item in UserInfo.instance.currentDeck.listCard)
            {
                if (listCard.ContainsKey(item.ID))
                {
                    listCard[item.ID].GetComponent<CardCover>().card.index = item.index + 1;
                    listCard[item.ID].GetComponent<CardCover>().offset = offset;
                    int indexF = currentDeck.listCard.FindIndex(x => x.ID == item.ID);
                    if (indexF != -1)
                        currentDeck.listCard[indexF] = listCard[item.ID].GetComponent<CardCover>().card;
                    listCard[item.ID].GetComponent<Toggle>().isOn = true;
                }
            }
            refreshIndex();
        }
    }

    void OnDisable()
    {
        resetDeck();
        tagSelect.Click -= OnAddTag;
        tagSelected.Click -= OnRemoveTag;
    }

    void OnEditCancel()
    {
        if (tempDeck != null)
        {
            UserInfo.instance.currentDeck = DeckDetail.getCopy(tempDeck);
            resetDeck();
        }
        ControlPanel.instance.ChangePage(0);
    }

    void OnCallBackGetListCard(bool status)
    {
        if(status)
        {
            
        }
    }

    void OnEditCardInDeck()
    {
        int lenght = currentDeck.listCard.Count;
        int[] cards = new int[lenght];

        for (int i = 0; i < lenght; i++)
        {
            cards[i] = currentDeck.listCard[i].ID;
        }

        int[] oldCards = new int[0];
        if (UserInfo.instance.currentDeck != null)
        {
            oldCards = new int[UserInfo.instance.currentDeck.listCard.Count];
            int a = 0;

            foreach (Card c in UserInfo.instance.currentDeck.listCard)
            {
                oldCards[a] = c.ID;
                a++;
            }
        }

        int[] AddList = cards.Except(oldCards).ToArray();

        int[] AddListIndex = new int[AddList.Length];


        if (AddListIndex.Length > 0)
        {
            int cCard = 0;
            for (int i = 0; i < currentDeck.listCard.Count; i++)
            {
                if (currentDeck.listCard[i].ID == AddList[cCard])
                {
                    AddListIndex[cCard] = currentDeck.listCard[i].index - 1;
                    cCard++;
                }
            }
        }

        int[] removeList = oldCards.Except(cards).ToArray();

        int[] dIDs = new int[removeList.Length];

        int IDDeck = currentDeck.ID;
        if (AddList.Length > 0)
        {
            CallApi.instance.callBegin("addCardInDeck");
            WebServiceManager.instance.AddCardInDeck(currentDeck.ID, AddList, AddListIndex, (success) =>
             {
                 CallApi.instance.callBack("addCardInDeck", success, (success2) =>
                   {
                       if (success2)
                           removeCard(IDDeck, dIDs, removeList);
                   });
             });
        }
        else
        {
            print("Add list == 0");
            removeCard(IDDeck, dIDs, removeList);
        }
    }

    void removeCard(int IDDeck, int[] dIDs, int[] removeList)
    {
        int count = 0;
        foreach (int i in removeList)
        {
            foreach (Card c in UserInfo.instance.currentDeck.listCard)
            {
                if (i == c.ID)
                {
                    dIDs[count] = c.dID;
                    CallApi.instance.callBegin("removeCardInDeck");
                    WebServiceManager.instance.ClearCardInDeck(c.dID, (success) =>
                    {
                        CallApi.instance.callBack("removeCardInDeck", success, (success2) =>
                          {
                              if (success2)
                              {
                                  CallApi.instance.callBegin("GetListCardInDeck");
                                  WebServiceManager.instance.GetListCardInDeck(IDDeck,
                                                                        (success3) =>
                                                                        {
                                                                            CallApi.instance.callBack("GetListCardInDeck", success3);
                                                                            if (success3)
                                                                                EventOnDone();
                                                                        });
                              }
                          });
                    });
                    break;
                }
            }
        }

        if (removeList.Length == 0)
        {
            print("remove list == 0");
            CallApi.instance.callBegin("GetListCardInDeck");
            WebServiceManager.instance.GetListCardInDeck(IDDeck,
                                                  (success) =>
                                                  {
                                                      CallApi.instance.callBack("GetListCardInDeck", success, (success2) =>
                                                      {
                                                          if (success)
                                                              EventOnDone();
                                                      });

                                                  });
        }
    }

    void OnEditNameDeck(WebServiceManager.callback callback = null)
    {
        print("OnEditNameDeck");
        if (UserInfo.instance.currentDeck == null)
        {
            CallApi.instance.callBegin("EditName");
            string nameNewDeck = "";
            if (currentDeck.name.Length > 0)
                nameNewDeck = "" + currentDeck.name;
            else
                nameNewDeck = "New Deck";
            WebServiceManager.instance.editDeck(currentDeck.ID.ToString(),
                                                nameNewDeck,
                                                "test"
                                                , (success) =>
                                                {
                                                    CallApi.instance.callBack("EditName", success, (success2) =>
                                                    {
                                                        if (!success2)
                                                            return;

                                                        if (callback != null)
                                                            callback();
                                                    });
                                                });
        }
        else if (currentDeck.name != UserInfo.instance.currentDeck.name)
        {
            print("Edit Name");
            CallApi.instance.callBegin("EditName");
            WebServiceManager.instance.editDeck(currentDeck.ID.ToString(),
                                                currentDeck.name,
                                                "test"
                                                , (success) =>
             {
                 CallApi.instance.callBack("EditName", success, (success2) =>
                  {
                      if (!success2)
                          return;
                      EditDeckInfoName(currentDeck.ID);
                      if (callback != null)
                          callback();
                  });
             });
        }
        else
        {
            if (callback != null)
                callback();
        }
    }

    void EditDeckInfoName(int id)
    {
        foreach (DeckListInfo Deck in UserInfo.instance.deckListInfos)
        {
            if (int.Parse(Deck.id) == id)
            {
                Deck.deck_name = currentDeck.name;
                break;
            }
        }
    }

    private bool OnValidateSaveDeck(string NameDeck,int nOfCard)
    {
        bool pass = false;
        string msgError = "";

        if (NameDeck == null)
            NameDeck = "";
        
        if(NameDeck.Length <= 0)
        {
            msgError += "กรุณาตั้งชื่อกลุ่ม ด้วยค่ะ\n";
        }

        if(nOfCard <= 0)
        {
            msgError += "กรุณาเลือกการ์ด ด้วยค่ะ\n";;
        }

        pass = msgError.Length <= 0;
        if (!pass)
            MessageBox.instance.showError(msgError);
        
        return pass;
    }

    void OnEditDone()
    {
        print("Edit Done");
        if (UserInfo.instance.currentDeck == null)
        {
            print("name : " + currentDeck.name);
            print("n card : " + currentDeck.listCard.Count);
            print("CreateNewDeck");
        }
        else
        {
            print("name : " + currentDeck.name);
            print("n card : " + currentDeck.listCard.Count);
            print("EditDeck");
        }

        if (!OnValidateSaveDeck(currentDeck.name,currentDeck.listCard.Count))
            return;
        
        MessageBox.instance.showMsg("คุณแน่ใจที่จะบันทึกข้อมูล",() =>
        {
            if (UserInfo.instance.currentDeck == null)
            {
                CallApi.instance.callBegin("CreateNewDeck");
                CallApi.instance.callBegin("CreateNewDeck");
                List<string> listID = new List<string>();
                List<string> newlistID = new List<string>();
                foreach (DeckListInfo d in UserInfo.instance.deckListInfos)
                {
                    listID.Add(d.id);
                }
                //print("NewDone");
                WebServiceManager.instance.newDeck(currentDeck.name, "test", UserInfo.instance.User.organization_id, UserInfo.instance.User.department_id,
                                               (success) =>
                                               {
                                                   if (success)
                                                   {
                                                       
                                                       CallApi.instance.callBack("CreateNewDeck", success);
                                                       WebServiceManager.instance.LoadDeckList(UserInfo.instance.User.department_id, UserInfo.instance.User.organization_id, (success3) =>
                                                       {
                                                           CallApi.instance.callBack("CreateNewDeck", success3, (success2) =>
                                                                                                                          {
                                                                                                                              if (success2)
                                                                                                                              {
                                                                                                                                  foreach (DeckListInfo d in UserInfo.instance.deckListInfos)
                                                                                                                                  {
                                                                                                                                      newlistID.Add(d.id);
                                                                                                                                  }
                                                                                                                                  string[] List = newlistID.Except(listID).ToArray();

                                                                                                                                  if (List.Length > 0)
                                                                                                                                  {
                                                                                                                                      //print(List[0]);
                                                                                                                                      currentDeck.ID = int.Parse(List[0]);
                                                                                                                                        
                                                                                                                                      OnEditNameDeck(OnEditCardInDeck);
                                                                                                                                  }
                                                                                                                              }
                                                                                                                          });
                                                       });
                                                   }

                                               });
            }
            else
            {
                OnEditNameDeck(OnEditCardInDeck);
            }
        });
    }
    void EventOnDone()
    {

        print("Deck : " + currentDeck.name);
        print("Cards : " + currentDeck.listCard.Count);
        UserInfo.instance.currentDeck = DeckDetail.getCopy(currentDeck);
        if (ValueChange != null)
            ValueChange(DeckDetail.getCopy(currentDeck));
        resetDeck();
        ControlPanel.instance.ChangePage(0);    
    }

    void resetDeck()
    {
        CardCover.ValueChange -= OnSelectCard;
        tempDeck = null;
        currentDeck = null;
        resetToggle();
    }

    void resetToggle()
    {
        Toggle[] t = content.GetComponentsInChildren<Toggle>();
        foreach (Toggle item in t)
        {
            if (item.isOn)
                item.isOn = false;
        }
    }

    public void OnEditName()
    {
        currentDeck.name = nameText.text;
    }

    [SerializeField] List<GameObject> listC;
    List<string> listTag = new List<string>();

    void OnUnaddTag(string name)
    {
        int c = listTag.Count;
        if (c > 0)
        {
            string[] tags = listTag.ToArray();
            foreach (string tag in tags)
            {
                if(tag != name)
                    OnRemoveTag(tag);
            }
        }
    }

    void OnAddTag(string name)
    {
        OnUnaddTag(name);
        int i = listTag.FindIndex(x => x == name);
        if (i == -1)
        {
            int indexTag = Array.FindIndex(UserInfo.instance.cardTypeByIds, x => x.id == name);
            nameOfTag.text = "" + UserInfo.instance.cardTypeByIds[indexTag].name;
            listTag.Add(name);
        }
        else
        {
            return;
            nameOfTag.text = "";
            OnRemoveTag(name);
        }
        setFollowTag();
        OnTagColor(listTag1[name], i == -1);
        OnTagColor(listTag3[name], i == -1);

        if(scrollRect != null && scrollRect.verticalScrollbar != null)
            scrollRect.verticalScrollbar.value = 1.0f;
    }

    void OnRemoveTag(string name)
    {
        int i = listTag.FindIndex(x => x == name);
        if (i != -1)
            listTag.Remove(name);
        setFollowTag();
        //print("i : " + i + "  || name : " + name);
        //listTag3[name].SetActive(true);
        //listTag1[name].SetActive(true);
        OnTagColor(listTag1[name], false);
        OnTagColor(listTag3[name], false);
    }

    void OnTagColor(GameObject obj,bool on)
    {
        float colorRGB = 0;
        if(on)
        {
            colorRGB = 85.0f;
        }
        else
        {
            colorRGB = 141.0f;
        }
        colorRGB /= 255.0f;
        obj.GetComponent<Image>().color = new Color(colorRGB, colorRGB, colorRGB, 255);
    }

    public void refreshTag()
    {
        filterPanel.SetActive(!filterPanel.active);
        foreach (var item in listTag1.Keys)
        {
            listTag3[item].SetActive(listTag1[item].active);
        }
    }

    public void setFollowTag()
    {
        //print("listC : " + listC.Count);
        //print("listCard : " + listCard.Count);
        if (listTag.Count <= 0)
        {
            foreach (GameObject c in listCard.Values)
            {
                c.SetActive(true);
            }
        }
        else
        {
            listC = listCard.Values.ToList();
            foreach (string t in listTag)
            {
                listC = readTag(t);
            }
        }
    }

    List<GameObject> readTag(string tag)
    {
        List<GameObject> listCurrent = new List<GameObject>();
        foreach (GameObject c in listC)
        {
            c.SetActive(false);
            CardCover cc = c.GetComponent<CardCover>();
            //print("On read Tag : " + cc.card.type + "   tag : " + tag);
            if (tag == cc.card.type)
            {
                listCurrent.Add(c);
                c.SetActive(true);
            }
        }
        return listCurrent;
    }
}
