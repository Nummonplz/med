﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ProfileData
{
    public string username;
    public string password;
    public bool autoLogin;

    public ProfileData()
    {
        username = "";
        password = "";
        autoLogin = false;
    }
}


public class SaveAndLoad {

    public static SaveAndLoad instance;

    public static List<DeckList> data = new List<DeckList>();

    public static ProfileData profile = new ProfileData();

    public static void Save(DeckList listDeck)
    {
        data = new List<DeckList>();
        data.Add(listDeck);
        string filename = Application.dataPath + @"\Resources\" + "result.dat";
        FileStream fileStream = File.Create(filename);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(fileStream, data);
        fileStream.Close();
    }

    public static void Load()
    {
        string filename = Application.dataPath + @"\Resources\" + "result.dat";
        if (File.Exists(filename))
        {
            FileStream fileStream = File.Open(filename, FileMode.Open, FileAccess.Read);
            BinaryFormatter formatter = new BinaryFormatter();
            data = (List<DeckList>)formatter.Deserialize(fileStream);
            fileStream.Close();
        }
    }

    public static void SaveProfile(string username,string password,bool autoLogin)
    {
        PlayerPrefs.SetString("username",username);
        PlayerPrefs.SetString("password",password);
        PlayerPrefs.SetString("autoLogin",autoLogin.ToString());
    }

    public static void SaveDataCards(string id,string version)
    {
        PlayerPrefs.SetString("Card"+id, version);
    }

    public static void SaveDataAtlast(string id, string version)
    {
        PlayerPrefs.SetString("Atlast" + id, version);
    }

    public static string LoadDataCards(string id)
    {
        string version = "-1";
        if (PlayerPrefs.HasKey("Card" + id))
            version = PlayerPrefs.GetString("Card" + id);
        return version;
    }

    public static string LoadDataAtlast(string id)
    {
        string version = "-1";
        if (PlayerPrefs.HasKey("Atlast" + id))
            version = PlayerPrefs.GetString("Atlast" + id);
        return version;
    }

    public static void LoadProfile()
    {
        profile = new ProfileData();
        if(PlayerPrefs.HasKey("username"))
        {
            string username = PlayerPrefs.GetString("username");
            profile.username = username;
        }
        else
        {
            PlayerPrefs.SetString("username", profile.username);
        }

        if (PlayerPrefs.HasKey("password"))
        {
            string password = PlayerPrefs.GetString("password");
            profile.password = password;
        }
        else
        {
            PlayerPrefs.SetString("password", profile.password);
        }

        if (PlayerPrefs.HasKey("autoLogin"))
        {
            string autoLogin = PlayerPrefs.GetString("autoLogin");
            profile.autoLogin = autoLogin == "True" ? true : false;
        }
        else
        {
            PlayerPrefs.SetString("autoLogin", profile.autoLogin.ToString());
        }
        MonoBehaviour.print("u : " + profile.username);
        MonoBehaviour.print("p : " + profile.password);
        MonoBehaviour.print("a : " + profile.autoLogin.ToString());
    }
}
