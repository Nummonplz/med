﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardSelect : MonoBehaviour
{
    [SerializeField] Image cardImage;
    [SerializeField] Image cardPoint;
    [SerializeField] Text cardName;

    public Sprite[] imgs;

    public delegate void OnButton(int id);
    public static event OnButton OnClick;
    Button button;

    int index;

    void Start()
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(delegate { OnClickSelect(); });
    }

    public void setUp(string name, Sprite spr,int i)
    {
        index = i;
        cardImage.sprite = spr;
        cardName.text = name;
    }

    public void setUpPoint(Point point)
    {
        if (cardPoint == null)
            return;
        
        cardPoint.gameObject.SetActive(true);
        switch (point)
        {
            case Point.correct:
                if(imgs[0])
                    cardPoint.sprite = imgs[0];
                break;
            case Point.incorrect:
                if(imgs[1])
                    cardPoint.sprite = imgs[1];
                break;
            default:
                cardPoint.gameObject.SetActive(false);
                break;
        }
    }
    

    void OnClickSelect()
    {
        if(OnClick != null)
        {
            OnClick(index);
        }
    }
}
