﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleOT : MonoBehaviour
{
    Toggle toggle;
    public OTTreatments oT;

    public void setUp()
    {
        toggle = this.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate { OnClick(); });
    }

    public OTTreatments setOT(string name,bool on)
    {
        if (toggle != null)
            toggle.isOn = on;
        if (oT == null)
            oT = new OTTreatments();
        oT.name = name;
        oT.on = on;
        return oT;
    }

    void OnClick()
    {
        if(oT != null)
        {
            oT.on = toggle.isOn;
        }
    }

}


public class createToggle : MonoBehaviour {

    [SerializeField] Transform content;
    [SerializeField] GameObject prefap;
    List<Toggle> listToggle;
    [SerializeField] List<OTTreatments> OTTreatments;
    public string[] allToggle;

	public void setUp ()
    {
        if (listToggle != null)
            return;
        OTTreatments = new List<OTTreatments>();
        listToggle = new List<Toggle>();
        int c = allToggle.Length;
        for (int i = 0; i < c; i++)
        {
            CreateToggle(i);
        }
    }

    void CreateToggle(int i)
    {
        GameObject newToggle = Instantiate(prefap);
        newToggle.transform.SetParent(content,false);
        newToggle.transform.GetChild(1).GetComponent<Text>().text = "" + allToggle[i];
        Toggle t = newToggle.GetComponent<Toggle>();
        t.isOn = false;
        listToggle.Add(t);
        newToggle.AddComponent<ToggleOT>();
        newToggle.GetComponent<ToggleOT>().setUp();
        OTTreatments newOT = newToggle.GetComponent<ToggleOT>().setOT("" + i, t.isOn);       
        OTTreatments.Add(newOT);
    }

    public List<OTTreatments> resetOT()
    {
        List<OTTreatments> ot = new List<OTTreatments>();
        for (int i = 0; i < allToggle.Length; i++)
        {
            OTTreatments newOT = new OTTreatments();
            newOT.name = allToggle[i];
            newOT.on = false;
            ot.Add(newOT);
        }
        return ot;
        
        
    }

    public void resetToggle(List<OTTreatments> ot)
    {
        if (listToggle == null)
            setUp();
        OTTreatments = ot;
        for (int i = 0; i < ot.Count; i++)
        {
            listToggle[i].gameObject.GetComponent<ToggleOT>().oT = ot[i];
            listToggle[i].isOn = ot[i].on;
        }
    }
}
