﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Point
{
    correct,
    incorrect,
    none
}

public enum Emotion
{
    Good,
    Normal,
    Bad
}

public class CardGame
{
    public int index;
    public string id;
    public string nameTH;
    public string nameEN;
    public Sprite sprite;
    public Point point;
    public Emotion emote;
    public float time;
    public QuestionPotential[] Questions = new QuestionPotential[4];
}

public class SessionData
{
    public int no;
    public int currentIndex;
    public int maxCard;
    public List<CardGame> cards;
    public Emotion emotion;
}

public class QuestionPotential
{
    public int id;
    public string title;
    public Point point;
}

public class GameEmotion
{
    
}

public class SessionControl : MonoBehaviour
{
    public static SessionControl instance;
    [SerializeField] Button buttonBack;
    [SerializeField] Button buttonEnd;
    [SerializeField] Button buttonQuestion;
    public Deck deck;
    public SessionData sessionData;
    public GamePlayCover gamePlay;
    public ChoiceEvent choiceEvent;
    public ChoiceEmotion choiceEmotion;

    public Text cardsText;
    public RectTransform[] RectImage;
    float swipeRange;

    public GameObject[] TopAndBottom;
    public Transform range;

    public QuestionPotentialManager questionPotentialManager;

    public VisualResults visualResults;

    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
        swipeRange = ((Screen.width / 100) * 10);
        buttonBack.onClick.AddListener(delegate { OnBack(); } );
        buttonEnd.onClick.AddListener(delegate { OnEndSession(); }); 
        buttonQuestion.onClick.AddListener(delegate { OnQuestion(); }); 
    }

    void OnEnable()
    {
        visualResults.gameObject.SetActive(false);
        CardSelect.OnClick += OnCard;
        OnCard(sessionData.currentIndex);
    }
    
    void OnDisable()
    {
        CardSelect.OnClick -= OnCard;
    }

    public void setDeck(Deck outDeck)
    {
        deck = outDeck;
        CreateCardGame();
        sessionData.emotion = Emotion.Normal;
        choiceEmotion.setCover(sessionData);
    }


    int offset = 0;
    List<int> ids;
    void CreateCardGame()
    {
        sessionData = new SessionData();
        if (UserInfo.instance.patientData.HistorySessionData != null && UserInfo.instance.patientData.HistorySessionData.Count > 0)
        {
            sessionData.no = UserInfo.instance.patientData.HistorySessionData.Count + 1;
        }
        else
            sessionData.no = 1;
        sessionData.cards = new List<CardGame>();
        sessionData.currentIndex =  0;
        sessionData.maxCard = deck.listCard.Count;
        int i = 0;
        offset = 0;
        ids = new List<int>();
        foreach (Card c in deck.listCard)
        {
            CardGame cardGame = new CardGame();
            if (UserInfo.instance.cardsInfo.ContainsKey(c.ID.ToString()))
            {
                CardInfo cardInfo = UserInfo.instance.cardsInfo[c.ID.ToString()];
                cardGame.index = i;
                cardGame.id = c.ID.ToString();
                cardGame.nameTH = cardInfo.card_name;
                cardGame.nameEN = cardInfo.card_name_en;
                cardGame.emote = Emotion.Normal;
                cardGame.sprite = UserInfo.instance.cardsInfo[c.ID.ToString()].img;

                cardGame.point = Point.none;
                for (int j = 0; j < cardGame.Questions.Length; j++)
                {
                    cardGame.Questions[j] = new QuestionPotential();
                    cardGame.Questions[j].point = Point.none;
                }
                sessionData.cards.Add(cardGame);
                ++i;
            }
            else
            {
                offset++;
                ids.Add(c.ID);
            }
           
        }
        WebServiceManager.instance.SendScore(UserInfo.instance.patientData.pt.userid, deck.ID.ToString(), null, null, null, null,
                                             delegate
                                             {
                                             });
    }

    public void OnNextCard()
    {
        if (sessionData.currentIndex >= sessionData.maxCard-1-offset)
            return;
        ++sessionData.currentIndex;
        OnCard(sessionData.currentIndex);        
    }

    public void OnPreviousCard()
    {
        if (sessionData.currentIndex <= 0)
            return;
        --sessionData.currentIndex;
        OnCard(sessionData.currentIndex);
    }

    void OnCard(int index)
    {
        sessionData.currentIndex = index;
        gamePlay.setCover(sessionData.cards[sessionData.currentIndex]);
        numberText();

        choiceEvent.setCover(sessionData.cards[sessionData.currentIndex]);

        choiceEmotion.setCoverByCard(sessionData.cards[sessionData.currentIndex]);

        StopAllCoroutines();
        if(sessionData.cards[sessionData.currentIndex].point == Point.none)
            StartCoroutine(countTimeCardGame());
        if (questionPotentialManager.gameObject.active)
            OnQuestion();
    }

    public void OnStopTimeCount()
    {
        this.StopAllCoroutines();
    }

    IEnumerator countTimeCardGame()
    {
        while (true)
        {
            sessionData.cards[sessionData.currentIndex].time += Time.deltaTime;
            yield return null; 
        }
    }

    void numberText()
    {
        cardsText.text = string.Format(" {0} / {1} ",sessionData.currentIndex + 1,sessionData.maxCard-offset);
    }

    Vector2 positionTemp;
    float timeTemp = 0;
    float delay = 0;
    bool doubleClick = false;

    void Update()
    {
        if (delay <= 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                /*if (Time.time - timeTemp < 0.2f && Vector2.Distance(Input.mousePosition,positionTemp) < swipeRange/2)
                {
                    Ontexttospeech();
                    doubleClick = true;
                    delay = 0.5f;
                }*/

                timeTemp = Time.time;
                positionTemp = Input.mousePosition;

            }
            else if (Input.GetMouseButtonUp(0))
            {
                /*if (doubleClick)
                {
                    doubleClick = false;
                    return;
                }*/

                if (Time.time - timeTemp < 0.5f)
                {
                    if (Input.mousePosition.x - positionTemp.x > swipeRange)
                    {
                        OnPreviousCard();
                        positionTemp = new Vector2();
                        delay = 0.5f;
                    }
                    else if (Input.mousePosition.x - positionTemp.x < -swipeRange)
                    {
                        OnNextCard();
                        positionTemp = new Vector2();
                        delay = 0.5f;
                    }
                    else if (Input.mousePosition.y - positionTemp.y < -swipeRange || Input.mousePosition.y - positionTemp.y > swipeRange)
                    {
                        bool active = TopAndBottom[0].activeSelf;
                        TopAndBottom[0].SetActive(!active);
                        TopAndBottom[1].SetActive(!active);
                        // Image set RectTransform 
                        int index = active ? 1 : 0;
                        gamePlay.image.transform.localScale = RectImage[index].transform.localScale;
                        gamePlay.image.GetComponent<RectTransform>().anchorMax = RectImage[index].anchorMax;
                        gamePlay.image.GetComponent<RectTransform>().anchorMin = RectImage[index].anchorMin;
                        gamePlay.image.GetComponent<RectTransform>().pivot = RectImage[index].pivot;
                        gamePlay.image.GetComponent<RectTransform>().rect.Set(0,0,0,0);
                        // Text set fontSize and RectTransform
                        gamePlay.nameText.GetComponent<RectTransform>().anchorMax = RectImage[index + 2].anchorMax;
                        gamePlay.nameText.GetComponent<RectTransform>().anchorMin = RectImage[index + 2].anchorMin;
                        if(RectImage[index + 2].gameObject.GetComponent<Text>())
                            gamePlay.nameText.GetComponent<Text>().fontSize = RectImage[index + 2].gameObject.GetComponent<Text>().fontSize;
                    }
                }
            }
        }
        else
        {
            if(delay > 0)
                delay -= Time.deltaTime;
        }
    }

    public void Ontexttospeech()
    {
        string word = "";
        string language = "";
        switch (gamePlay.language)
        {
            case Language.EN:
                word = "" + sessionData.cards[sessionData.currentIndex].nameEN;
                language = "en";
                break;
            case Language.TH:
                word = "" + sessionData.cards[sessionData.currentIndex].nameTH;
                language = "th";
                break;

        }
        SoundManager.instance.texttospeech( word, language);
    }
    
    void OnQuestion()
    {
        questionPotentialManager.showQP(sessionData.cards[sessionData.currentIndex]);
    }
    
    void OnBack()
    {
        ControlPanel.instance.ChangePage(0);
    }

    bool CanEndSession()
    {
        int score = 0;
        foreach (CardGame c in sessionData.cards)
        {
            switch (c.point)
            {
                case Point.correct:
                    score += 1;
                    break;
                case Point.incorrect:
                    score += 1;
                    break;
                default:
                    break;
            }
        }

        if(score == 0)
        {
            MessageBox.instance.showError("คุณยังไม่ได้เล่นการ์ด");
        }

        return (score == 0);
    }

    void OnEndSession()
    {
        if (CanEndSession())
            return;
        MessageBox.instance.showMsg("คุณแน่ใจไหมว่าจะจบเกม", () =>
        {
            int n = sessionData.cards.Count;
            int count = 0;
            foreach (CardGame c in sessionData.cards)
            {
                int score = -1;
                int scoreEmote = -1;
                switch (c.point)
                {
                    case Point.correct:
                        score = 1;
                        break;
                    case Point.incorrect:
                        score = 0;
                        break;
                    case Point.none:
                        score = -1;
                        break;
                    default:
                        break;
                }

                switch (c.emote)
                {
                    case Emotion.Good:
                        scoreEmote = 1;
                        break;
                    case Emotion.Bad:
                        scoreEmote = 0;
                        break;
                    case Emotion.Normal:
                        scoreEmote = -1;
                        break;
                    default:
                        break;
                }

                count++;
                print("Emote : " + scoreEmote.ToString() + "      time : " + c.time.ToString());
                WebServiceManager.instance.SendScore(UserInfo.instance.patientData.pt.userid, deck.ID.ToString(), c.id, score.ToString(), scoreEmote.ToString(), c.time.ToString(),
                                                     delegate
                                                     {
                                                         if (count >= n)
                                                             refreshScoreList();
                                                     });
            }
            foreach (int index in ids)
            {
                WebServiceManager.instance.SendScore(UserInfo.instance.patientData.pt.userid, deck.ID.ToString(), index.ToString() , "-1", "-1", "0",
                                                     delegate
                                                     {
                                                         if (count >= n)
                                                             refreshScoreList();
                                                     });
            }
            UserInfo.instance.sessionData = this.sessionData;
            ControlPanel.instance.ChangePage(4);
        });
    }

    void refreshScoreList()
    {
        WebServiceManager.instance.ScoreList(UserInfo.instance.patientData.pt.userid, deck.ID.ToString(), deck.name,
                                             delegate
                                             {
                                             });
    }


}
