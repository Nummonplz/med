﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UserInfo : MonoBehaviour {

    public static UserInfo instance;

    public DoctorCover doctorCover;
    public PatientData patientData;
    public SessionData sessionData;
    public SelectDeckHandler selectDeckHandler;
    public Deck currentDeck;


    // test
    public User User;
    public DeckListInfo[] deckListInfos;
    public Deck[] decks;
    public Patient[] patients;
    public List<ScoreList> scoreLists;

    // data Gmae
    public CardTypeById[] cardTypeByIds;

    // sprite card
    /*[SerializeField]
    public Dictionary<string, Sprite> listSprite;*/

    [SerializeField]
    public Dictionary<string, CardInfo> cardsInfo;

    public bool Logined;
    public bool SkipLoadImage;
    public bool SoundOn = true;
    void Start ()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
        init();
    }

    void init()
    {
        //listSprite = new Dictionary<string, Sprite>();
        cardsInfo = new Dictionary<string, CardInfo>();
    }
}
