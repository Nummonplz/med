﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoctorData
{
    public string name;
    public List<PatientData> patientDatas;
}
    


public class DoctorCover : MonoBehaviour
{
    DoctorData doctorData;
    public Image profileImage;
    public Text nameCompanyText;
    public Text nameText;
    public Transform content;
    public GameObject patientPrefap;
    public GameObject NotFoundText;

    public GameObject LoginObj;

    public InputField inputID;
    void Start ()
    {
        UIDisplay();
        doctorData = new DoctorData();
        doctorData.patientDatas = new List<PatientData>();
        refreshPatientList();
    }

    void UIDisplay()
    {
        nameText.text = UserInfo.instance.User.name;
        nameCompanyText.text = UserInfo.instance.User.organization_name;
    }

    public void refreshPatientList()
    {
        clearList();
        foreach (Patient pt in UserInfo.instance.patients)
        {
            CreatePatient(pt);
        }
    }

    void CreatePatient(Patient pt = null)
    {
        GameObject newPatient = Instantiate(patientPrefap);
        newPatient.transform.SetParent(content,false);
        newPatient.transform.SetSiblingIndex(content.transform.childCount - 2);
        newPatient.GetComponent<PatientCover>().pd = new PatientData();
        PatientData patientData = newPatient.GetComponent<PatientCover>().pd;
        patientData.pt = pt;
        newPatient.GetComponent<PatientCover>().obj = newPatient;
        patientData.patientCover = newPatient.GetComponent<PatientCover>();
        doctorData.patientDatas.Add(patientData);
        inputID.text = "";
        showList("");
    }

    public void sortName()
    {
        doctorData.patientDatas.Sort((x, y) => string.Compare(x.pt.name, y.pt.name));
        for (int i = 0; i < doctorData.patientDatas.Count; i++)
        {
            doctorData.patientDatas[i].patientCover.obj.transform.SetSiblingIndex(i);
        }
    }


    public void DeletePatient(PatientData patient)
    {
        int i = doctorData.patientDatas.IndexOf(patient);
        if(i != -1)
        {
            doctorData.patientDatas.RemoveAt(i);
            Destroy(content.transform.GetChild(i).gameObject);
        }
        inputID.text = "";
        showList("");
    }

    void clearList()
    {
        int c = content.transform.childCount;
        for (int i = 0; i < c; i++)
        {
            Destroy(content.transform.GetChild(i).gameObject);
        }
        doctorData.patientDatas = new List<PatientData>();
        inputID.text = "";
        showList("");
    }

    public void showList(string input)
    {
        input = inputID.text.ToLower();
        int cFoundData = 0;
        foreach (PatientData pd in doctorData.patientDatas)
        {
            bool on;
            if (input.Length > 0)
            {
                if (pd.pt.name.ToLower().Contains(input) || pd.pt.hn.ToLower().Contains(input))
                {
                    on = true;
                    cFoundData++;
                }
                else
                {
                    on = false;
                }
            }
            else
                on = true;
            
            pd.patientCover.obj.SetActive(on);
        }
        if(input.Length > 0)
            NotFoundText.SetActive(cFoundData == 0);
        else
            NotFoundText.SetActive(false);

        sortName();
    }

    private void OnEnable()
    {
        if (doctorData == null)
            return;
        inputID.text = "";
        showList("");
        NotFoundText.SetActive(false);
    }

    public void OnLogout()
    {
        if (UserInfo.instance.Logined)
            return;
        MessageBox.instance.showMsg("คุณแน่ใจที่จะออกจากระบบ", () =>
        {
            SaveAndLoad.SaveProfile("", "", false);
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        });
    }

    public void refresh()
    {
        if (LoginObj == null)
            return;
        LoginPanel LoginPanelObj = LoginObj.GetComponent<LoginPanel>();

        if (UserInfo.instance.Logined)
            return;

        MessageBox.instance.showMsg("คุณแน่ใจที่จะปรับข้อมูลเป็นปัจจุบันหรือไม่", delegate 
        {
            UserInfo.instance.Logined = true;
            if (LoginPanelObj != null)
            {
                print("Found");
                LoginPanelObj.refreshLogin();
                ControlPanel.instance.ChangePage(5);
            }
            else
            {
                print("Not Found");
            }
        });
    }

}
