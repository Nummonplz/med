﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonAns : MonoBehaviour
{

    public delegate void OnValueChange(bool on,string name);
    public static event OnValueChange ValueChange;
    public string Name;
    Toggle button;
    
	void Start ()
    {
        button = this.GetComponent<Toggle>();
        button.onValueChanged.AddListener(delegate { OnClick(); });
    }


    void OnClick()
    {
        if(ValueChange != null)
        {
            ValueChange(button.isOn,this.Name);
        }
    }
	


}
