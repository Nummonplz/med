﻿using UnityEngine;
using UnityEngine.UI;

public enum Language
{
    TH,
    EN
}

public class GamePlayCover : MonoBehaviour
{
    public Text nameText;
    public Image image;
    CardGame card;
    public Language language;

    [SerializeField] Transform Toggles;

    void OnEnable()
    {
        ToggleLanguage.ValueChange += setToggle;
        initToggle(Toggles.GetComponentsInChildren<Toggle>());     
    }

    void OnDisable()
    {
        ToggleLanguage.ValueChange -= setToggle;
    }

    public void setToggle(Toggle toggle)
    {
        if (toggle == null)
            return;
        print(toggle.name + " toggle.isOn : " + toggle.isOn.ToString());
        switch (toggle.name)
        {
            case "Show":            
                nameText.gameObject.SetActive(toggle.isOn);
                break;
            case "TH/EN":
                ChangeLangue(toggle);
                nameText.text = language == Language.TH ? card.nameTH : card.nameEN;
                break;
            case "SoundOn":
                UserInfo.instance.SoundOn = toggle.isOn;
                break;
            case "ImageOn/Off":
                image.gameObject.SetActive(toggle.isOn);
                break;
            default:
                break;
        }
    }

    void initToggle(Toggle[] toggles)
    {
        foreach (Toggle toggle in toggles)
        {
            switch (toggle.name)
            {
                case "Show":
                    toggle.isOn = true;
                    break;
                case "TH/EN":
                    toggle.isOn = true;
                    break;
                case "SoundOn":
                    toggle.isOn = true;
                    break;
                case "ImageOn/Off":
                    toggle.isOn = true;
                    break;
                default:
                    break;
            }
        }
    }

    public void ChangeLangue(Toggle toggle)
    {
        if (toggle.isOn)
            language = Language.TH;
        else
            language = Language.EN;        
    }

    public void setCover(CardGame c)
    {
        card = c;
        image.sprite = c.sprite;
        nameText.text = language == Language.TH ? card.nameTH : card.nameEN;
    }
}
