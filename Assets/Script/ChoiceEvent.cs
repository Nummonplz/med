﻿using UnityEngine;
using UnityEngine.UI;

public class ChoiceEvent : MonoBehaviour
{

    public VisualResults visualResults;
    CardGame cardGame;
    public Toggle[] Toggles;
    public void ClickButton(bool on,string name)
    {
        print("Name : " + name + " || On : " + on);
        if(!on && !Toggles[0].isOn && !Toggles[1].isOn)
        {
            cardGame.point = Point.none;
            print("Score none");
        }

        if (!on)
            return;
        switch (name)
        {
            case "0":
                if (cardGame.point != Point.incorrect)
                {
                    visualResults.gameObject.SetActive(true);
                    visualResults.ShowVisual(int.Parse(name));
                    SoundManager.instance.PlaySound(1);
                }
                if (cardGame != null)
                    cardGame.point = Point.incorrect;
                Toggles[1].isOn = !on;
                SessionControl.instance.OnStopTimeCount();
                break;
            case "1":
                if (cardGame.point != Point.correct)
                {
                    visualResults.gameObject.SetActive(true);
                    visualResults.ShowVisual(int.Parse(name));
                    SoundManager.instance.PlaySound(0);
                }
                if(cardGame!= null)
                    cardGame.point = Point.correct;
                Toggles[0].isOn = !on;
                SessionControl.instance.OnStopTimeCount();
                break;
            default:
                break;
        }
    }

    void OnDisable()
    {
        ButtonAns.ValueChange -= ClickButton;
    }

    void OnEnable()
    {
        ButtonAns.ValueChange += ClickButton;
    }

    public void setCover(CardGame cg)
    {
        cardGame = cg;
        switch (cardGame.point)
        {
            case Point.correct:
                Toggles[1].isOn = true;
                break;
            case Point.incorrect:
                Toggles[0].isOn = true;
                break;
            case Point.none:
                Toggles[0].isOn = false;
                Toggles[1].isOn = false;
                break;
            default:
                break;
        }
    }

}

