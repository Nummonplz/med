﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectDeckHandler : MonoBehaviour
{
    PatientData pd;
    [SerializeField] Transform content;
    
    public Deck currentDeck;
    public Toggle currentToggle;


    Dictionary<int, GameObject> listDeck = new Dictionary<int, GameObject>();
    [SerializeField] Button addButton;
    [SerializeField] Button editButton;
    [SerializeField] Button deleteButton;
    [SerializeField] Button backButton;
    [SerializeField] Button startButton;
    [SerializeField] SessionControl sessionControl;

    [SerializeField] Text name;
    [SerializeField] Text Hn;
    public GameObject GuideCreateDeck;

    void Start()
    {
        EditDeck.ValueChange += OnSaveDeck;

        addButton.onClick.AddListener(delegate
        {
            CreateNewDeck();
        });

        deleteButton.onClick.AddListener(delegate
        {
            DeleteDeck();
        });

        editButton.onClick.AddListener(delegate { OnEditDeck(); });
        backButton.onClick.AddListener(delegate { Back(); });
        startButton.onClick.AddListener(delegate { StartSession(); });
    }

    public void setDeckList(PatientData PD)
    {
        OnScreenLayout();
        pd = PD;
        int c = content.transform.childCount;
        if (c > 0)
        {
            for (int i = 0; i < c; i++)
            {
                Destroy(content.transform.GetChild(i).gameObject);
            }
            listDeck.Clear();
        }
        clearDeck();

        Deck[] decks;
        decks = ConvertDeck();
        UserInfo.instance.decks = decks;
        /*if (UserInfo.instance.decks.Length == 0)
        {
            decks = ConvertDeck();
            UserInfo.instance.decks = decks;
        }
        else
        {
            
        }*/

        if (decks.Length >= 0)
        {
            foreach (Deck d in decks)
            {
                CreateDeck(d);
            }
        }



        /*if (pd.deckList != null && pd.deckList.listDeck.Count >= 0)
        {
            foreach (Deck d in pd.deckList.listDeck)
            {
                CreateDeck(d);
            }
        }
        else
            pd.deckList = new DeckList();
        */
    }

    void OnScreenLayout()
    {
        //string xy = string.Format("h: {0} w: {1}",Screen.height,Screen.width);
        float h = 0;
        float w = 0;
        h = Camera.main.aspect * 130;
        w = (h / 3) * 4;
        content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(h,w);
    }

    void UIName()
    {
        if (UserInfo.instance.patientData == null)
            return;
        
        name.text = "NAME : " + UserInfo.instance.patientData.pt.name;
        Hn.text = "HN/AN : " + UserInfo.instance.patientData.pt.hn;
    }

    void OnEnable()
    {
        //currentDeck = null;
        UIName();
        setDeckList(this.pd);
        DeckCover.ValueChange += OnSelectDeck;
        if (UserInfo.instance.deckListInfos.Length == 0)
        {
            GuideCreateDeck.SetActive(UserInfo.instance.deckListInfos.Length == 0);
            GuideCreateDeck.GetComponent<Button>().onClick.AddListener(delegate {
                GuideCreateDeck.SetActive(false);            
            });
        }

        inputDeckName.text = "";
        showList("");
    }

    Deck[] ConvertDeck()
    {
        Deck[] decks = new Deck[UserInfo.instance.deckListInfos.Length];
        int i = 0;

        foreach (DeckListInfo deckInfo in UserInfo.instance.deckListInfos)
        {
            decks[i] = new Deck();
            decks[i].ID = int.Parse(deckInfo.id);
            decks[i].name = deckInfo.deck_name;
            decks[i].listCard = new List<Card>();
            int ic = 0;
            if (deckInfo.cardInfos != null)
            {
                foreach (CardInfo card in deckInfo.cardInfos)
                {
                    //print(card.id);
                    Card c = new Card();
                    c.ID = int.Parse(card.card_id);
                    c.dID = int.Parse(card.id);
                    c.name = card.card_name;
                    c.index = int.Parse(card.index);
                    decks[i].listCard.Add(c);

                    if (UserInfo.instance.cardsInfo.ContainsKey(card.card_id))
                    {
                                           
                    }
                    else
                    {
                        ic++;
                    }
                }
                decks[i].offset = ic;
                deckInfo.offset = ic;
            }
            i++;
        }
        return decks;
    }


    void OnDisable()
    {
        DeckCover.ValueChange -= OnSelectDeck;
    }

    void CreateNewDeck()
    {
        /*WebServiceManager.instance.newDeck("newDeck", "test", UserInfo.instance.User.organization_id, UserInfo.instance.User.department_id,
                                               delegate
                                               {
                                                   WebServiceManager.instance.LoadDeckList(UserInfo.instance.User.department_id, UserInfo.instance.User.organization_id, delegate
                                                                                          {
                                                                                              setDeckList(this.pd);
                                                                                          });
                                               });*/
        UserInfo.instance.currentDeck = null;
        ControlPanel.instance.ChangePage(1);
    }

    public void CreateDeck(Deck deck = null)
    {
        GameObject newDeck = Instantiate(LoadResources.Load("prefap/Deck"));
        newDeck.transform.SetParent(content,false);
        DeckCover dk = newDeck.GetComponent<DeckCover>();

        if (deck == null)
        {
            /*DeckDetail.addDeck(pd.deckList,dk.deck);
            listDeck[dk.deck.ID] = newDeck;
            OnSelectDeck(dk.deck,dk.toggle);
            OnEditDeck();*/
        }
        else
        {
            dk.deck = deck;
            listDeck[dk.deck.ID] = newDeck;
        }
        dk.refreshCover();
    }

    void OnSelectDeck(Deck deck,Toggle toggle)
    {
        int i = -1;
        for (int j = 0; j < UserInfo.instance.decks.Length; j++)
        {
            if(UserInfo.instance.decks[j] == deck)
            {
                i = j;
                break;
            }
        }
        //int i = pd.deckList.listDeck.FindIndex(x => x.ID == deck.ID);
        /*if (i != -1)
            deck = pd.deckList.listDeck[i];*/
        if (i != -1)
            deck = UserInfo.instance.decks[i];
        if (currentDeck != null && currentDeck.ID == deck.ID)
        {
            currentDeck = null;
            currentToggle = null;
            toggle.isOn = false;
        }
        else if (currentToggle != null && currentDeck.ID != deck.ID)
        {
            currentToggle.isOn = false;
            currentToggle = toggle;
            currentDeck = deck;
        }
        else
        {
            currentToggle = toggle;
            currentDeck = deck;
        }

    }

    void OnEditDeck()
    {
        if (currentDeck == null)
            return;
        UserInfo.instance.currentDeck = currentDeck;
        ControlPanel.instance.ChangePage(1);
    }

    void OnSaveDeck(Deck deck)
    {
        int c = UserInfo.instance.decks.Length;

        for (int i = 0; i < c; i++)
        {
            if(UserInfo.instance.decks[i].ID == deck.ID)
            {
                UserInfo.instance.decks[i] = deck;
                listDeck[deck.ID].GetComponent<DeckCover>().deck = deck;
                currentDeck = deck;
                break;
            }
        }
    }

    void StartSession()
    {
        if (currentDeck == null)
            return;
        if (currentDeck.listCard.Count <= 0)
            return;
        UserInfo.instance.currentDeck = currentDeck;
        sessionControl.setDeck(currentDeck);
        //currentDeck = null;
        //currentToggle = null;
        ControlPanel.instance.ChangePage(3);
    }

    void DeleteDeck()
    {
        if (currentDeck == null)
            return;
        MessageBox.instance.showMsg("คุณแน่ใจที่จะลบข้อมูลหรือไม่",OnDeleteDeck);
    }

    void OnDeleteDeck()
    {
        CallApi.instance.callBegin("DeleteDeck");
        WebServiceManager.instance.deleteDeck(currentDeck.ID.ToString(),
                                           (success) =>
                                           {
                                               CallApi.instance.callBack("DeleteDeck", success);
                                               CallApi.instance.callBegin("ListDeck");
                                               WebServiceManager.instance.LoadDeckList(UserInfo.instance.User.department_id, UserInfo.instance.User.organization_id,
                                                                                       (success2) =>
                                               {
                                                   CallApi.instance.callBack("ListDeck", success2, delegate
                                                   {
                                                        setDeckList(this.pd);
                                                   });
                                                   
                                               });

                                           });
    }

    void Back()
    {
        clearDeck();
        ControlPanel.instance.ChangePage(2);
    }

    public void clearDeck()
    {
        currentDeck = null;
        currentToggle = null;
    }

    public InputField inputDeckName;

    public void showList(string input)
    {
        input = inputDeckName.text.ToLower();
        //print("input : " + input);
        foreach (var deck in listDeck)
        {
            bool on;
            if (input.Length > 0)
            {
                //print(deck.Value.GetComponent<DeckCover>().deck.name);
                if (deck.Value.GetComponent<DeckCover>().deck.name.Contains(input))
                {
                    on = true;
                }
                else
                {
                    on = false;
                }
            }
            else
                on = true;

            deck.Value.SetActive(on);
        }
    }

}
