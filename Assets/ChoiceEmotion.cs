﻿using UnityEngine;
using UnityEngine.UI;

public class ChoiceEmotion : MonoBehaviour
{
    [SerializeField] GameObject Area;
    SessionData gameEmotion;
    CardGame cardGame;

    public Sprite[] sprites;
    public void ClickButton(string name)
    {
        switch (name)
        {
            case "-1":
                gameEmotion.emotion = Emotion.Bad;
                cardGame.emote = Emotion.Bad;
                changeSprite(0);
                print("Bad");
                break;
            case "0":
                gameEmotion.emotion = Emotion.Normal;
                cardGame.emote = Emotion.Normal;
                changeSprite(1);
                break;
            case "1":
                gameEmotion.emotion = Emotion.Good;
                cardGame.emote = Emotion.Good;
                changeSprite(2);
                print("Good");
                break;
            default:
                break;
        }
        Area.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        ButtonEmotion.ValueChange -= ClickButton;
    }

    void OnEnable()
    {
        ButtonEmotion.ValueChange += ClickButton;
    }

    public void setCover(SessionData ge)
    {
        gameEmotion = ge;
        switch (gameEmotion.emotion)
        {
            case Emotion.Good:
                changeSprite(2);
                break;
            case Emotion.Bad:
                changeSprite(0);
                break;
            case Emotion.Normal:
                changeSprite(1);
                break;
            default:
                break;
        }
    }

    public void setCoverByCard(CardGame cardEmote)
    {
        cardGame = cardEmote;
        switch (cardEmote.emote)
        {
            case Emotion.Good:
                changeSprite(2);
                break;
            case Emotion.Bad:
                changeSprite(0);
                break;
            case Emotion.Normal:
                changeSprite(1);
                break;
            default:
                break;
        }
    }

    void Update()
    {
        if (Area.activeSelf)
            HideIfClickedOutside(Area.gameObject);
    }

    private void HideIfClickedOutside(GameObject panel)
    {
        if (Input.GetMouseButton(0) && panel.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                panel.GetComponent<RectTransform>(),
                Input.mousePosition,
                Camera.main))
        {
            panel.SetActive(false);
        }
    }

    void changeSprite(int i)
    {
        if(i == 0)
            this.GetComponent<Image>().color = Color.red;
        else if(i == 2)
            this.GetComponent<Image>().color = Color.green;
        else if (i == 1)
            this.GetComponent<Image>().color = Color.white;
        this.GetComponent<Image>().sprite = sprites[i];
    }
}
