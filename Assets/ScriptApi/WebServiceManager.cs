﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.IO;
using System;
public class WebServiceManager : MonoBehaviour 
{
    public static WebServiceManager instance;
    public ResultsData<User> results;

    public delegate void callback();
    public delegate void callbackApi(bool success);
    void Start () 
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    public void OnLogin(string username, string password,LoginPanel.LoginCallback callback = null)
    {
        StartCoroutine(CheckLogin(username,password,callback));
    }

    IEnumerator CheckLogin(string username,string password,LoginPanel.LoginCallback callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/CheckLogin";
        form.AddField("username",username);
        form.AddField("password",password);
        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
            {
                callback(false, www.error);
            }
        }
        else
        {
            checkLogin(www.text,callback);
        }

    }

    void checkLogin(string json,LoginPanel.LoginCallback callback = null)
    {
        json = json.Trim('[', ']');
        results = JsonUtility.FromJson<ResultsData<User>>(json);
        //print(json);
        UserInfo.instance.User = results.data;
        if (results.status)
        {
            StartCoroutine(WWWLoadImage(results.data, 256, 256));
            LoadDeckList(UserInfo.instance.User.department_id, UserInfo.instance.User.organization_id);
        }

        if(callback != null)
        {
            callback(results.status,json);
        }
    }

    IEnumerator WWWLoadImage(User user,int width,int height)
    {
        WWW page = new WWW(user.pic);
        yield return page;

        if (page.error != null)
        {
            UnityEngine.Debug.LogWarning("Error: " + page.error);
        }
        else
        {
            Texture2D image = new Texture2D(width, height);           
            image.filterMode = FilterMode.Point;
            if (image.LoadImage(page.bytes))
            {
                user.image = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.one * 0.5f);
            }
            else
            {
                //UnityEngine.Debug.LogWarning("Error: Could not load image");              
            }
        }
    }

    void saveImage(string path, byte[] imageBytes)
    {
        //Create Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        try
        {
            File.WriteAllBytes(path, imageBytes);
            //UnityEngine.Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            //UnityEngine.Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
            //UnityEngine.Debug.LogWarning("Error: " + e.Message);
        }
    }

    byte[] loadImage(string path)
    {
        byte[] dataByte = null;
        
        //Exit if Directory or File does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            //UnityEngine.Debug.LogWarning("Directory does not exist");
            return null;
        }

        if (!File.Exists(path))
        {
            //UnityEngine.Debug.Log("File does not exist");
            return null;
        }

        try
        {
            dataByte = File.ReadAllBytes(path);
            //UnityEngine.Debug.Log("Loaded Data from: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            //UnityEngine.Debug.LogWarning("Failed To Load Data from: " + path.Replace("/", "\\"));
            //UnityEngine.Debug.LogWarning("Error: " + e.Message);
        }

        return dataByte;
    }

    private static string GetAndroidFriendlyFilesPath()
    {
#if UNITY_EDITOR
        return Application.persistentDataPath;
#endif
#if UNITY_ANDROID
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject applicationContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaObject path = applicationContext.Call<AndroidJavaObject>("getFilesDir");
        string filesPath = path.Call<string>("getCanonicalPath");
        return filesPath;
#endif     
        return Application.persistentDataPath;
    }
    public static Texture2D Resize(Texture2D source, int newWidth, int newHeight)
    {
        source.filterMode = FilterMode.Point;
        RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
        rt.filterMode = FilterMode.Point;
        RenderTexture.active = rt;
        Graphics.Blit(source, rt);
        Texture2D nTex = new Texture2D(newWidth, newHeight,TextureFormat.RGBA32,false);
        nTex.ReadPixels(new Rect(0, 0, newWidth, newWidth), 0, 0);
        nTex.Apply();
        RenderTexture.ReleaseTemporary(rt);
        RenderTexture.active = null;               
        Destroy(source);
        return nTex;
    }

    IEnumerator WWWLoadCardImage(CardInfo card, int width, int height)
    {
        CallApi.instance.callBegin("cardsImageLoading");
        Texture2D image = new Texture2D(width, height, TextureFormat.DXT1, true);     
        string url = Path.Combine(GetAndroidFriendlyFilesPath() + "/Img/", card.id + ".png");
        yield return null;     
        string CardVersion = SaveAndLoad.LoadDataCards(card.id);
        bool ForceLoadImage = false;
        if (CardVersion == "-1")
        {
            SaveAndLoad.SaveDataCards(card.id,card.version);
            ForceLoadImage = true;
        }
        else
        {
            if(CardVersion == card.version)
            {
                ForceLoadImage = false;
            }
            else
            {
                SaveAndLoad.SaveDataCards(card.id, card.version);
                ForceLoadImage = true;
            }
        }
        //print(card.card_name + " v. : " + card.version + " Update : " + ForceLoadImage);
        yield return null;
        if (!ForceLoadImage && loadImage(url) != null && image.LoadImage(loadImage(url)))
        {                   
            image.filterMode = FilterMode.Point;
            Texture2D newImage = Resize(image, width, height);           
            images.Add(newImage);
            Destroy(image);
            /*card.img = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.one);
            if (card.cardCover != null)
                card.cardCover.setUp(card.card_name, int.Parse(card.id), card.img, card.card_type);*/
            CallApi.instance.callBack("cardsImage", true);
            CallApi.instance.callBack("cardsImageLoading", true, delegate { LoadImageCardManage(); });           
            yield return null;
        }
        else
        {
            WWW page = new WWW(card.card_pic);
            //  START TIMER
            float K_F_NETWORK_TIMEOUT_MILLIS = 120000;//120000f;//120 SEC TIMEOUT
            Stopwatch oStopwatch = new Stopwatch();
            oStopwatch.Reset();
            oStopwatch.Start();
            bool bTimedOut = false;

            //  WAIT FOR WEB REQUEST
            for (; ; )
            {
                //fProgress = page.progress;
                //print("" + card.card_name + " : " + page.progress);
                if (page.isDone)
                {
                    break;
                }
                if (oStopwatch.ElapsedMilliseconds > K_F_NETWORK_TIMEOUT_MILLIS)
                {
                    bTimedOut = true;
                    break;
                }
                yield return null;
            }
            //  FINISHED
            if (!bTimedOut)
            {
                if (page.error != null)
                {
                    UnityEngine.Debug.LogWarning("Error: " + page.error);
                    CallApi.instance.callBack("cardsImageLoading", true, delegate { LoadImageCardManage(); });
                    CallApi.instance.callBack("cardsImage", true);
                }
                else
                {
                    if (image.LoadImage(page.bytes))
                    {
                        image.filterMode = FilterMode.Point;
                        Texture2D newImage = Resize(image, width, height);
                        images.Add(newImage);
                        Destroy(image);
                        saveImage(GetAndroidFriendlyFilesPath() + "/Img/" + card.id + ".png", page.bytes);
                        /*card.img = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.one * 0.5f);
                        if (card.cardCover != null)
                            card.cardCover.setUp(card.card_name, int.Parse(card.id), card.img, card.card_type);*/
                        CallApi.instance.callBack("cardsImageLoading", true, delegate { LoadImageCardManage(); });
                        CallApi.instance.callBack("cardsImage", true);
                        Resources.UnloadUnusedAssets();
                    }
                    else
                    {
                        CallApi.instance.callBack("cardsImageLoading", true, delegate { LoadImageCardManage(); });
                        CallApi.instance.callBack("cardsImage", true);
                        UnityEngine.Debug.LogWarning("Error: Could not load image");
                    }
                }
            }
            else
            {
                card.isLoaded = false;
                MessageBox.instance.showErrorEvent("โปรดตรวจสอบการเชื่อมต่ออินเตอร์เน็ต",
                delegate
                {
                    print("Load Failed");
                });
            }
            page.Dispose();
            page = null;
        }
    }

    public List<Texture2D> images;
    public List<Texture2D> AtlastImages;
    public List<Rect> r;
    Queue<CardInfo> cardInfosQueue;
    public void LoadImageCardManage()
    {
        float MaxRateTexture = 32.0f;
        for (int i = 0; i < 1; i++)
        {
            if (images.Count != 0 && (images.Count % MaxRateTexture == 0 || cardInfosQueue.Count == 0))
            {              
                int amount = Mathf.CeilToInt(images.Count / MaxRateTexture);
                Texture2D newAtlastImages = new Texture2D(0, 0, TextureFormat.DXT1,false);
                List<Texture2D> listAtlastImages = new List<Texture2D>();
                int IndexnewAtlast = (int)MaxRateTexture * (amount);
                for (int j = (int)MaxRateTexture * (amount - 1); j < (int)MaxRateTexture * (amount); j++)
                {
                    if (j >= images.Count)
                    {
                        IndexnewAtlast = j;                        break;
                    }
                    listAtlastImages.Add(images[j]);
                }
                r.AddRange(newAtlastImages.PackTextures(listAtlastImages.ToArray(), 0, 4096, false));
                AtlastImages.Add(newAtlastImages);
                //saveImage(GetAndroidFriendlyFilesPath() + "/Img/AtlastImage" + IndexnewAtlast + ".png", newAtlastImages.EncodeToPNG());
                StartCoroutine(CreateSpriteToCard(MaxRateTexture, amount, newAtlastImages));
            }
            else if (cardInfosQueue.Count > 0)
            {                
                StartCoroutine(WWWLoadCardImage(cardInfosQueue.Dequeue(), 512, 512));
            }
            else
            {              
                images.Clear();
                r.Clear();               
            }
        }
    }
    IEnumerator CreateSpriteToCard(float MaxRateTexture,int amount, Texture2D newAtlastImages)
    {
        CallApi.instance.callBegin("PackTextures");
        int offset = 0;
        for (int j = (int)MaxRateTexture * (amount - 1); j < (int)MaxRateTexture * (amount); j++)
        {
            if (j >= images.Count)
                break;
            if (j >= ids.Count)
                break;
            string index = ids[j];
            if (UserInfo.instance.cardsInfo.ContainsKey(index))
            {
                UserInfo.instance.cardsInfo[index].img = Sprite.Create(AtlastImages[amount - 1], new Rect(newAtlastImages.width * r[j].x, newAtlastImages.height * r[j].y, newAtlastImages.width * r[j].width, newAtlastImages.height * r[j].height), Vector2.one);
                if (UserInfo.instance.cardsInfo[index].cardCover != null)
                    UserInfo.instance.cardsInfo[index].cardCover.setUp(UserInfo.instance.cardsInfo[index].card_name, int.Parse(UserInfo.instance.cardsInfo[index].id), UserInfo.instance.cardsInfo[index].img, UserInfo.instance.cardsInfo[index].card_type);
            }
            yield return null;
            Destroy(images[j]);
            yield return null;
        }
        yield return null;
        Resources.UnloadUnusedAssets();
        System.GC.Collect();       
        if (cardInfosQueue.Count > 0)
            StartCoroutine(WWWLoadCardImage(cardInfosQueue.Dequeue(), 512, 512));
        CallApi.instance.callBack("PackTextures", true);
    }

    public void LoadDeckList(string departmentId, string organizationId, callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWDeckList(departmentId, organizationId,callback));
        else
            StartCoroutine(WWWDeckList(departmentId, organizationId));
    }

    IEnumerator WWWDeckList(string departmentId, string organizationId, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();

        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/DeckList";

        form.AddField("department_id", departmentId);
        form.AddField("organization_org_id", organizationId);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            callback(false);
        }
        else
        {
            string json = www.text;
            json = json.Trim('[', ']');
            if (!json.Contains("false"))
            {
                ResultsDatas<DeckListInfo> DeckList = JsonUtility.FromJson<ResultsDatas<DeckListInfo>>(json);
                UserInfo.instance.deckListInfos = DeckList.data;
                foreach (DeckListInfo item in DeckList.data)
                {
                    CallApi.instance.callBegin("cardsDeck");
                    GetListCardInDeck(int.Parse(item.id),
                    (success) =>
                    {
                        CallApi.instance.callBack("cardsDeck", success, (success2) =>
                        {
                            if(success2)
                                if (callback != null)
                                    callback(true);
                        });
                    });
                        
                }
            }
            else
            {
                UserInfo.instance.deckListInfos = new DeckListInfo[0];
                if (callback != null)
                    callback(true);
            }

        }
        yield return null;
    }

    public void AddCardInDeck(int deck_id,int[] card,int[] index,callbackApi callback = null)
    {
        StartCoroutine(WWWNewDeckOption(deck_id,card,index,callback));
    }

    IEnumerator WWWNewDeckOption(int deck_id, int[] card,int[] index,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();

        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/NewDeckOption";

        form.AddField("deck_id", deck_id.ToString());

        int c1 = card.Length;
        int c2 = index.Length;
        if(c1 == c2)
        {
            for (int i = 0; i < c1; i++)
            {
                print(index[i]);
                string field = string.Format("card[{0}]", index[i]);
                form.AddField(field, card[i].ToString());
                print(field + " : " + card[i].ToString());
            }
        }

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
                callback(false);
        }
        else
        {
            print(www.text);
            if (callback != null)
                callback(true);
        }

        yield return null;
    }

    public void ClearCardInDeck(int id,callbackApi callback = null)
    {
        StartCoroutine(WWWDeleteDeckOption(id,callback));
    }

    IEnumerator WWWDeleteDeckOption(int id,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();

        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/DeleteDeckOption";
        form.AddField("id", id.ToString());
        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
                callback(false);
        }
        else
        {
            //print(www.text);
            if (callback != null)
                callback(true);
        }

        yield return null;
    }

    public void GetListCardInDeck(int deck_id,callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWDeckOptionList(deck_id,callback));
        else
            StartCoroutine(WWWDeckOptionList(deck_id));
    }

    IEnumerator WWWDeckOptionList(int id,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();

        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/DeckOptionList";

        form.AddField("deck_id", id.ToString());

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print("Deck Option Error : " + www.error);
            if (callback != null)
            {
                callback(false);
            }
        }
        else
        {
            string json = www.text;
            json = json.Trim('[', ']');
//            print("DeckOption Sucess " + json);
            ResultsDatas<CardInfo> CardInDeckArray;
            if (json.Contains("true"))
                CardInDeckArray = JsonUtility.FromJson<ResultsDatas<CardInfo>>(json);
            else
                CardInDeckArray = new ResultsDatas<CardInfo>();
            
            if (CardInDeckArray.data != null)
            {
                foreach (DeckListInfo i in UserInfo.instance.deckListInfos)
                {
                    if(i.id == id.ToString())
                    {
                        i.cardInfos = CardInDeckArray.data;
                        break;
                    }
                }
            }

            if (callback != null)
            {
                callback(true);
            }
        }
    }

    public void SendScore(string userid, string deckid, string card_id = null, string score = null, string emote = null, string time = null, callback callback = null)
    {
        if (card_id != null && score != null && emote != null && time != null)
        {
            StartCoroutine(WWWScore(userid, deckid, card_id, score, emote, time, callback));
            //print("SendScore");
        }
        else
        {
            StartCoroutine(WWWScore(userid, deckid, null, null, null, null, callback));
            //print("StartSendScore");
        }
    }

    IEnumerator WWWScore(string user_id,string deck_id,string card_id = null,string score = null,string emote = null,string time = null,callback callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/Score";
        form.AddField("uid", user_id);
        form.AddField("deck_id", deck_id);
        if(card_id != null)
            form.AddField("card_id", card_id);
        if(score != null)
            form.AddField("score", score);
        if (emote != null)
            form.AddField("emote", emote);
        if (time != null)
            form.AddField("time", time);
        WWW www = new WWW(url, form);
        yield return www;

        if (www.error != null)
        {
            print(www.text);
            print(www.error);
        }
        else
        {
            if (callback != null)
                callback();
            print(www.text);
        }
        yield return null;
    }

    public void getCards(string org_id,callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWCardList(org_id,callback));
        else
            StartCoroutine(WWWCardList(org_id));
    }
    public List<string> ids;
    IEnumerator WWWCardList(string org_id,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/CardList";

        form.AddField("org_id", org_id);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            if (callback != null)
                callback(true);
        }
        else
        {
            string json = www.text;
            if(AtlastImages != null && AtlastImages.Count > 0)
            {
                foreach (Texture2D t in AtlastImages)
                {
                    if (t != null)
                        Destroy(t);
                }
            }
            AtlastImages = new List<Texture2D>();
            if (images != null && images.Count > 0)
            {
                foreach (Texture2D t in images)
                {
                    if (t != null)
                        Destroy(t);
                }
            }
            images = new List<Texture2D>();
            r = new List<Rect>();
            cardInfosQueue = new Queue<CardInfo>();
            ids = new List<string>();
            json = json.Trim('[', ']');
            if (json.Contains("true"))
            {
                ResultsDatas<CardInfo> Cards = JsonUtility.FromJson<ResultsDatas<CardInfo>>(json);
                //print("Cardlist n : " + UserInfo.instance.cardsInfo.Count);
                foreach (CardInfo item in Cards.data)
                {
                    //print("id : " + item.id);
                    //print("name : " + item.card_name);
                    //print("link : " + item.card_pic);
                    if (!UserInfo.instance.cardsInfo.ContainsKey(item.id))
                    {
                        item.card_name = item.card_name.Trim();
                        UserInfo.instance.cardsInfo[item.id] = item;
                        
                    }

                    ids.Add(item.id);

                    if (!UserInfo.instance.SkipLoadImage)
                    {                        
                        CallApi.instance.callBegin("cardsImage");
                        cardInfosQueue.Enqueue(UserInfo.instance.cardsInfo[item.id]);                    
                    }
                }
                LoadImageCardManage();
                if (callback != null)
                    callback(Cards.status);
            }
            else
            {
                if (callback != null)
                    callback(true);
            }

        }

        yield return null;
    }

    public void newDeck(string deck_name,string deck_type,string organization_org_id,string department_id, callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWnewDeck(deck_name,deck_type,organization_org_id,department_id,callback));
        else
            StartCoroutine(WWWnewDeck(deck_name, deck_type, organization_org_id, department_id));
    }


    // Deck

    IEnumerator WWWnewDeck(string deck_name, string deck_type, string organization_org_id, string department_id, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/NewDeck";

        form.AddField("deck_name", deck_name);
        form.AddField("deck_type", deck_type);
        form.AddField("organization_org_id", organization_org_id);
        form.AddField("department_id", department_id);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
                callback(false);
        }
        else
        {
            print(www.text);
            if (callback != null)
                callback(true);
        }

        yield return null;
    }

    public void editDeck(string id,string deck_name,string deck_type, callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWeditDeck(id,deck_name,deck_type,callback));
        else
            StartCoroutine(WWWeditDeck(id, deck_name, deck_type));
            
    }

    IEnumerator WWWeditDeck(string id, string deck_name, string deck_type, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/EditDeck";

        form.AddField("id", id);
        form.AddField("deck_name", deck_name);
        form.AddField("deck_type", deck_type);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            callback(false);
        }
        else
        {
            if (callback != null)
                callback(true);
        }
        yield return null;
    }

    public void deleteDeck(string id,callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWdeleteDeck(id, callback));
        else
            StartCoroutine(WWWdeleteDeck(id));
    }

    IEnumerator WWWdeleteDeck(string id, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/DeleteDeck";

        form.AddField("id", id);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            callback(false);
        }
        else
        {
            if (callback != null)
                callback(true);
        }
        yield return null;
    }

    public void CardTypeList(callbackApi callback = null)
    {
        if(callback != null)
            StartCoroutine(WWWCardTypeList(callback));
        else
            StartCoroutine(WWWCardTypeList());
    }

    IEnumerator WWWCardTypeList(callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/CardTypeList";
        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            if (callback != null)
                callback(false);
        }
        else
        {
            string json = www.text;
            json = json.Trim('[', ']');
            //print(" json : " + json);
            ResultsDatas<CardTypeById> Cards = JsonUtility.FromJson<ResultsDatas<CardTypeById>>(json);
            UserInfo.instance.cardTypeByIds = Cards.data;
            if (callback != null)
                callback(Cards.status);
        }
        yield return null;
    }

    // Patient

    public void NewPatient(string organization_org_id, string department_id, string lastname, string firstname, string hn, string phone, string sex, string age, string remark, callbackApi callback = null)
    {
        if (callback != null)
        {
            StartCoroutine(WWWNewUser(organization_org_id, department_id, lastname, firstname, hn, phone, sex, age, remark, callback));
        }
        else
        {
            StartCoroutine(WWWNewUser(organization_org_id, department_id, lastname, firstname, hn, phone, sex, age, remark));
        }
    }

    IEnumerator WWWNewUser(string organization_org_id, string department_id, string lastname, string firstname, string hn, string phone, string sex, string age, string remark, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/NewUser";

        form.AddField("organization_org_id", organization_org_id);
        form.AddField("department_id", department_id);
        form.AddField("lastname", lastname);
        form.AddField("firstname", firstname);
        form.AddField("hn", hn);
        form.AddField("phone", phone);
        form.AddField("gender", sex);
        form.AddField("age", age);
        form.AddField("remark", remark);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
                callback(false);
        }
        else
        {
            bool pass = false;
            string json = www.text;
            json = json.Trim('[', ']');
            if (json.Contains("false"))
            {
                json = json.Replace("[", "");
                json = json.Replace("]", "");
                json = json.Replace("},},", "}},");
                json = json.Replace("\"msg\":", "");
                json = json.Replace("{{", "{");
                json = json.Replace("}}", "}");

                print(json);
                ResultsData<msg> resultsMsg = JsonUtility.FromJson<ResultsData<msg>>(json);
                if (!resultsMsg.status)
                {
                    string msgError = "";
                    if (resultsMsg.data.username != null)
                        msgError += resultsMsg.data.username + "\n";
                    if (resultsMsg.data.email != null)
                        msgError += resultsMsg.data.email + "\n";
                    MessageBox.instance.showError(msgError);
                }
            }
            else
                pass = true;

            if (callback != null)
                callback(pass);
        }

        yield return null;
    }

    public void EditPatient(string uid, string lastname = null, string firstname = null, string hn = null, string phone = null, string sex = null, string age = null, string remark = null, callbackApi callback = null)
    {
        if (callback != null)
        {
            StartCoroutine(WWWEditUser(uid, lastname, firstname, hn, phone, sex, age, remark, callback));
        }
        else
        {
            StartCoroutine(WWWEditUser(uid, lastname, firstname, hn, phone, sex, age, remark));
        }
    }

    IEnumerator WWWEditUser(string uid, string lastname, string firstname, string hn, string phone,string sex,string age,string remark,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/EditUser";

        form.AddField("uid", uid);
        if(lastname != null)
            form.AddField("lastname", lastname);
        if(firstname != null)
            form.AddField("firstname", firstname);
        if(hn != null)
            form.AddField("hn", hn);
        if(phone != null)
            form.AddField("phone", phone);
        if (sex != null)
            form.AddField("gender", sex);
        if (age != null)
            form.AddField("age", age);
        if (remark != null)
            form.AddField("remark", remark);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            callback(false);
        }
        else
        {
            //print(www.text);
            string json = www.text;
            json = json.Trim('[', ']');
            ResultsData<string> results = JsonUtility.FromJson<ResultsData<string>>(json);
            //print(results.status);
            if (callback != null)
                callback(results.status);
        }

        yield return null;
    }

    public void DeletePatient(string uid,callbackApi callback = null)
    {
        if (callback != null)
        {
            StartCoroutine(WWWDeleteUser(uid,callback));
        }
        else
        {
            StartCoroutine(WWWDeleteUser(uid));
        }
    }

    IEnumerator WWWDeleteUser(string uid,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/DeleteUser";

        form.AddField("uid", uid);

        WWW www = new WWW(url, form);
        yield return www;
        if (www.error != null)
        {
            print(www.error);
            callback(false);
        }
        else
        {
            //print(www.text);
            string json = www.text;
            json = json.Trim('[', ']');
            ResultsData<string> results = JsonUtility.FromJson<ResultsData<string>>(json);
            //print(results.status);
            if (callback != null)
                callback(results.status);
        }

        yield return null;
    }

    public void PatientList(string admin_id,bool LoadScoreList,callbackApi callback = null)
    {
        if(callback != null)
        {
            StartCoroutine(WWWPatientList(admin_id, LoadScoreList, callback));
        }
        else
        {
            StartCoroutine(WWWPatientList(admin_id, LoadScoreList));
        }
    }

    IEnumerator WWWPatientList(string admin_id,bool LoadScoreList, callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/PatientList";

        form.AddField("admin_id", admin_id);

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error != null)
        {
            if (callback != null)
                callback(false);
        }
        else
        {
            string json = www.text;
            json = json.Trim('[', ']');
            //print(json);

            if (json.Length == 0)
            {
                UserInfo.instance.patients = new Patient[0];
                if (callback != null)
                    callback(true);
            }
            else
            {
                ResultsDatas<Patient> Patientlist = JsonUtility.FromJson<ResultsDatas<Patient>>(json);
                UserInfo.instance.patients = Patientlist.data;
                if (UserInfo.instance.deckListInfos.Length == 0 || Patientlist.data.Length == 0 || !LoadScoreList)
                {
                    if (callback != null)
                        callback(Patientlist.status);
                }
                else
                {
                    foreach (Patient pt in Patientlist.data)
                    {

                        CallApi.instance.callBegin("ScoreList", UserInfo.instance.deckListInfos.Length);
                        foreach (DeckListInfo deck in UserInfo.instance.deckListInfos)
                        {
                            float t = 0;

                            ScoreList(pt.userid, deck.id, deck.deck_name,
                                      (success) =>
                                      {
                                          t = 999;
                                          CallApi.instance.callBack("ScoreList",
                                                                    success,
                                                                    (success2) =>
                                                                    {
                                                                        if (callback != null && success2)
                                                                            callback(Patientlist.status);
                                                                    });
                                      });
                            while (t <= 100.0f)
                            {
                                t += Time.deltaTime;
                                yield return null;
                            }
                            yield return null;
                        }

                    }
                }
            }
        }
        yield return null;
    }

    public void ScoreList(string uid, string deck_id, string deck_name, callbackApi callback = null)
    {
        if (callback != null)
            StartCoroutine(WWWScoreList(uid, deck_id, deck_name, callback));
        else
            StartCoroutine(WWWScoreList(uid, deck_id, deck_name));
    }

    IEnumerator WWWScoreList(string uid,string deck_id,string deck_name,callbackApi callback = null)
    {
        WWWForm form = new WWWForm();
        string url = "27.254.63.96/senzeapp/service_game/index.php?r=Console/ScoreList";

        form.AddField("uid", uid);
        form.AddField("deck_id", deck_id);

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error != null)
        {
            print(www.error);
            if (callback != null)
                callback(false);
        }
        else
        {
            
            string json = www.text;
            json = json.Trim('[', ']');
            
            if (!json.Contains("false"))
            {
                ResultsDatas<ScoreList> ScoreList = JsonUtility.FromJson<ResultsDatas<ScoreList>>(json);
                foreach (ScoreList sc in ScoreList.data)
                {
                    sc.uid = uid;
                    sc.deckid = deck_id;
                    sc.deckname = deck_name;
                    int index = UserInfo.instance.scoreLists.FindIndex((x) => x.id == sc.id);
                    if(index == -1)
                        UserInfo.instance.scoreLists.Add(sc);
                }
            }

            if (callback != null)
                callback(true);

        }
        yield return null;
    }

}
