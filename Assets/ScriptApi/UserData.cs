﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserData : MonoBehaviour 
{

}
[System.Serializable]
public class User
{
    public string id;
    public string name;
    public string pic;
    public string organization_id;
    public string organization_name;
    public string department_id;
    public string department_name;
    public string birthday;
    public string hn;
    public string an;
    public string phone;
    public Sprite image;
}

[System.Serializable]
public class Patient
{
    public string userid;
    public string name;
    public string email;
    public string image;
    public string depart;
    public string hn;
    public string an;
    public string phone;

    public string gender;
    public string remark;
    public string age;
}

[System.Serializable]
public class msg
{
    public string email;
    public string username;
}

[System.Serializable]
public class DeckInfo
{
    public string organization_org_id;
    public string department_id;
    public string deck_name;
    public string deck_type;
}

[System.Serializable]
public class DeckListInfo
{
    public string id;
    public string deck_name;
    public string deck_type;
    public int offset;
    public CardInfo[] cardInfos;
}

[System.Serializable]
public class ResultsData<T>
{
    public T data;
    public bool status;
}

[System.Serializable]
public class ResultsDatas<T>
{
    public T[] data;
    public bool status;
}

[System.Serializable]
public class CardInfo
{
    public string id;
    public string index;
    public string card_id;
    public string card_name;
    public string card_name_en;
    public string card_type;
    public string card_pic;
    public string version;
    public Sprite img;
    public CardCover cardCover;
    public bool isLoaded;
}

[System.Serializable]
public class ScoreList
{
    public string uid;
    public string deckname;
    public string deckid;
    public string id;
    public string score;
    public string total;
    public string date;
}

[System.Serializable]
public class CardTypeById
{
    public string id;
    public string name;
}

[System.Serializable]
public class Wrapper<T>
{
    public T[] Items;
}


[System.Serializable]
public class DeckOption
{
    public string deck_id;
    public CardIndex[] card;
}


[System.Serializable]
public class CardIndex
{
    public string idcard;
    public string index;
}
