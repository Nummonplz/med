﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour 
{
    public static MessageBox instance;
    public delegate void CallbackMsg();
    CallbackMsg MsgCallback;
    //obj
    public Text msg;
    public Button y;
    public Button n;
    GameObject parentObj;

    //Error
    public Text errorMsg;
    GameObject errorObj;
    Coroutine errorCor;

    //ErrorEvent
    public Text errorMsgEvent;
    GameObject errorObjEvent;
    Coroutine errorCorEvent;

    void Start()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            parentObj = this.transform.GetChild(0).gameObject;
            errorObj = this.transform.GetChild(1).gameObject;
            errorObjEvent = this.transform.GetChild(2).gameObject;

            y.onClick.AddListener(
            delegate
            {
                Accept();
            });

            n.onClick.AddListener(
            delegate
            {
                Cancle();
            });

            Hide(false);
            HideError(false);
        }
    }

    void setup()
    {
        
    }

    void initError()
    {
        
    }

    public void showMsg(string title,CallbackMsg callback = null)
    {
        msg.text = title;
        Hide(true);
        if(callback != null)
        {
            MsgCallback = callback;
        }
    }

    public void showError(string msg)
    {
        HideError(true);
        errorCor = StartCoroutine(timingHideError());
        errorMsg.text = "" + msg;
    }

    public void showErrorEvent(string msg, CallbackMsg callback = null)
    {
        if (errorObjEvent.activeSelf)
            return;
        errorMsgEvent.text = "" + msg;
        errorObjEvent.SetActive(true);
        Button bErrorObjEvent = errorObjEvent.GetComponent<Button>();
        if (bErrorObjEvent != null)
        {
            print("Add Event");
            bErrorObjEvent.onClick.RemoveAllListeners();
            bErrorObjEvent.onClick.AddListener(delegate
            {
                print("Click");
                SaveAndLoad.SaveProfile("", "", false);
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                errorObjEvent.SetActive(false);
            });
        }
    }

    void Hide(bool on)
    {
        parentObj.SetActive(on);
    }

    void HideError(bool on)
    {
        errorObj.SetActive(on);
    }

    void Accept()
    {
        Hide(false);

        if(MsgCallback != null)
        {
            MsgCallback();
            MsgCallback = null;
        }
    }

    void Cancle()
    {
        Hide(false);
    }

    public void OnClickPanelError()
    {
        if (errorCor != null)
        {
            StopCoroutine(errorCor);
            errorCor = null;
        }
        HideError(false);
    }

    IEnumerator timingHideError()
    {
        yield return new WaitForSeconds(3.0f);
        if (errorObj != null)
            if (errorObj.activeSelf)
                HideError(false);
        yield return null;
    }

}
