﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryPanel : MonoBehaviour {

    public GameObject prefab;
    public Transform content;
    List<GameObject> allResult;
    //UI
    public Text[] topDataUI;

    private void OnEnable()
    {
        SetUp();
    }

    void SetTopUI()
    {
        topDataUI[0].text = "" + UserInfo.instance.patientData.pt.name;
        topDataUI[1].text = "HN : " + UserInfo.instance.patientData.pt.hn;
    }

    public void SetUp()
    {
        SetTopUI();
        clearTemp();
        createCardRusults();
    }

    List<ScoreList> GetScores()
    {
        List<ScoreList> scoreLists = new List<ScoreList>();
        foreach (ScoreList item in UserInfo.instance.scoreLists)
        {
            //print(item.uid);
            if (item.uid == UserInfo.instance.patientData.pt.userid)
                scoreLists.Add(item);
        }
        return scoreLists;
    }

    void createCardRusults()
    {
        List<ScoreList> scores = GetScores();
        foreach (ScoreList item in scores)
        {
            GameObject result = Instantiate(prefab);
            result.transform.SetParent(content, false);
            ScoreTable hp = result.GetComponent<ScoreTable>();
            string[] data = new string[5];
            string[] time = item.date.Split(' ');
            data[0] = time[0];
            data[1] = time[1];
            data[2] = item.deckname;
            data[4] = item.score + "/" + item.total;
            data[3] = "" + (int)(float.Parse(item.score)/float.Parse(item.total) * 100);
            hp.setUIData(data);
            allResult.Add(result);
        }
    }

    void clearTemp()
    {
        if (allResult == null)
            allResult = new List<GameObject>();
        int c = allResult.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(allResult[i].gameObject);
        }
    }

    public void Hide(bool hide)
    {
        this.gameObject.SetActive(!hide);
    }
}
