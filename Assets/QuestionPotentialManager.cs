﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionPotentialManager : MonoBehaviour 
{
    
    CardGame cardGame;
    public Transform list;

    public void showQP(CardGame cg)
    {
        cardGame = cg;
        int c = list.childCount;
        for (int i = 0; i < c; i++)
        {
            list.GetChild(i).GetComponent<QuestionPotentialCover>().setUI(cardGame.Questions[i]);
        }
        Hide(false);
    }

    public void Hide(bool hide)
    {
        this.gameObject.SetActive(!hide);
    }

}
