﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginPanel : MonoBehaviour
{
    public InputField ifUsername;
    public InputField ifPassword;
    public Button bLogin;

    public Text loginMsg;

    public delegate void LoginCallback(bool status, string msg);
    LoginCallback OnLoginCallback;


    private void OnEnable()
    {
        if (UserInfo.instance != null)
            if (UserInfo.instance.Logined)
                return;
        AutoLogin();
    }

    void Start()
    {
        //OnLoginCallback += ResultsLogin;
        bLogin.onClick.AddListener(delegate
        {
            bLogin.interactable = false;
            OnLogin();
        });
    }

    void ResultsLogin(bool status, string msg)
    {
        CallApi.instance.callBack("Login", status);
        if (status)
        {
            CallApi.instance.callBegin("AfterLogin");
            CallApi.instance.callBegin("AfterLogin");
            CallApi.instance.callBegin("AfterLogin");
            WebServiceManager.instance.CardTypeList((success) =>
            {
                //print("CardType " + success);
                checkLoginSuccess(success);
            }
            );
            WebServiceManager.instance.getCards(UserInfo.instance.User.organization_id,(success) =>
            {
                //print("getCards " + success);
                checkLoginSuccess(success);
            });
            WebServiceManager.instance.PatientList(UserInfo.instance.User.id, true,
                                                   (success) =>
                                                   {
                                                       //print("PatientList " + success);
                                                       checkLoginSuccess(success);
                                                   });
        }
        else
        {
            UserInfo.instance.Logined = false;
            bLogin.interactable = true;
            loginMsg.text = "กรุณาระบุช่ือผู้ใช้งาน และ พาสเวิร์ด ให้ถูกต้อง";
        }
    }

    void checkLoginSuccess(bool status)
    {
        CallApi.instance.callBack("AfterLogin",status, (success) =>
        {
            loginMsg.text = "";
            ControlPanel.instance.ChangePage(2);
            UserInfo.instance.Logined = false;
        });
    }

    void OnLogin()
    {
        string username = ifUsername.text;
        string password = ifPassword.text;
        CallApi.instance.callBegin("Login");
        WebServiceManager.instance.OnLogin(username, password, ResultsLogin);
        SaveAndLoad.SaveProfile(username,password,true);
    }

    void AutoLogin()
    {
        SaveAndLoad.LoadProfile();
        if(SaveAndLoad.profile.autoLogin)
        {
            ifUsername.text = SaveAndLoad.profile.username;
            ifPassword.text = SaveAndLoad.profile.password;
            StartCoroutine(OnLoging());
        }
    }

    IEnumerator OnLoging()
    {
        bLogin.interactable = false;
        yield return new WaitForSeconds(1.0f);
        OnLogin();
    }

    public void refreshLogin()
    {
        SaveAndLoad.LoadProfile();
        if (SaveAndLoad.profile.autoLogin)
        {
            ifUsername.text = SaveAndLoad.profile.username;
            ifPassword.text = SaveAndLoad.profile.password;
        }
        OnLogin();
    }

}
