﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTable : MonoBehaviour 
{
    Text[] Texts;

    public void setUIData(string[] msg)
    {
        int n = this.transform.childCount;
        Texts = new Text[n]; 
        for (int i = 0; i < n; i++)
        {
            if (msg.Length > i)
            {
                GameObject obj = this.transform.GetChild(i).gameObject;
                Texts[i] = obj.transform.GetChild(0).gameObject.GetComponent<Text>();
                Texts[i].text = msg[i];
            }
        }
    }
}
