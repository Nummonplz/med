﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChartManager : MonoBehaviour 
{
    public ChartCover[] ChartCover;
    public Text nameDeck;
    List<ScoreList> GetScores()
    {
        List<ScoreList> scoreLists = new List<ScoreList>();
        string IdDeck = ""+UserInfo.instance.currentDeck.ID;
        foreach (ScoreList item in UserInfo.instance.scoreLists)
        {
            //print("c deck:" + IdDeck + " tDeck:" + item.deckid);
            if (item.uid == UserInfo.instance.patientData.pt.userid && item.deckid == IdDeck)
                scoreLists.Add(item);
        }
        return scoreLists;
    }

    private void OnEnable()
    {
        nameDeck.text = string.Format("Deck({0})",UserInfo.instance.currentDeck.name);
        List<ScoreList> scoreLists = GetScores();
        ClearTemp();
        int start = scoreLists.Count - 1;
        int end = start - 4;

        if (start < 0)
            start = -1;
        if(end <= 0)
            end = -1;
        
        for (int j = 0; j < 4; j++)
        {
            print("Start"+start);
            if (start > -1 && start > end)
            {
                ChartCover[j].gameObject.SetActive(true);
                string[] time = scoreLists[start].date.Split(' ');
                string date = "" + time[0] + "      " + time[1];
                string score = "" + scoreLists[start].score;
                string total = "" + scoreLists[start].total;
                CalScore(date,int.Parse(score),int.Parse(total), j);
            }
            else
            {
                ChartCover[j].gameObject.SetActive(false);
            }
            start--;
        }
    }

    void CalScore(string date,int score,int total,int i)
    {
        int per = Mathf.RoundToInt((100.0f/total) * score);
        ChartCover[i].setUI(date,per/100f,per);
    }

    void ClearTemp()
    {
        for (int i = 0; i < ChartCover.Length; i++)
        {
            ChartCover[i].setUI("", 0, 0);
            ChartCover[i].gameObject.SetActive(false);
        }
    }

    public void Hide(bool on)
    {
        this.gameObject.SetActive(!on);
    }

}
